package mbds.tpt.pari.models;

public class Id {
    private long idProgramme;
    private  long idEquipe;

    public Id() {
    }

    public Id(long idProgramme, long idEquipe) {
        this.idProgramme = idProgramme;
        this.idEquipe = idEquipe;
    }

    public long getIdProgramme() {
        return idProgramme;
    }

    public void setIdProgramme(long idProgramme) {
        this.idProgramme = idProgramme;
    }

    public long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(long idEquipe) {
        this.idEquipe = idEquipe;
    }
}
