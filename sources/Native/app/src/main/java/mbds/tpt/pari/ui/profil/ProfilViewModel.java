package mbds.tpt.pari.ui.profil;

import android.content.SharedPreferences;
import android.widget.TextView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Collections;

import mbds.tpt.pari.R;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.models.Utilisateur;

import static mbds.tpt.pari.constants.Util.TOKEN;
import static mbds.tpt.pari.constants.Util.USERS_ID;

public class ProfilViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ProfilViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Mon Profil");
    }

    public LiveData<String> getText() {
        return mText;
    }
}