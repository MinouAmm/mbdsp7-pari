package mbds.tpt.pari.models;

public class Parametre {
    private long id;
    private String libelle;

    public Parametre() {
    }

    public Parametre(long id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
