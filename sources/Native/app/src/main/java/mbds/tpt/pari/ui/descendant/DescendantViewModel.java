package mbds.tpt.pari.ui.descendant;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DescendantViewModel extends ViewModel {

    private final MutableLiveData<String> mText;

    public DescendantViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Mes descendants");
    }

    public LiveData<String> getText() {
        return mText;
    }
}