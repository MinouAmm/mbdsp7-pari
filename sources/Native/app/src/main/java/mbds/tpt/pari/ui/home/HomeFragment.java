package mbds.tpt.pari.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.MainActivity;
import mbds.tpt.pari.R;
import mbds.tpt.pari.adapter.CategorieAdapter;
import mbds.tpt.pari.adapter.CategorieParentAdapter;
import mbds.tpt.pari.adapter.CustomDataBindingAdapter;
import mbds.tpt.pari.adapter.GridItemDecoration;
import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.models.Categorie;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.ProgrammeResponse;
import static mbds.tpt.pari.MainActivity.bearer;
import static mbds.tpt.pari.constants.PathApi.GET_PROGRAMME;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
    View root;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        RecyclerView recycler = root.findViewById(R.id.recycler_view);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false));
        int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        recycler.addItemDecoration(new GridItemDecoration(largePadding, smallPadding));
        //homeViewModel.getEvents().observe(getViewLifecycleOwner(), events -> CustomDataBindingAdapter.createList(getContext(), root, new EventAdapter(events)));
        return root;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        retrieveData();
    }

    private void createListeEvenement(View view) {
       // CustomDataBindingAdapter.createList(getContext(), view, new EventAdapter(new ArrayList<>()));
    }
    public void createGridProgramme(ProgrammeResponse[] cats) {
        RecyclerView recycler = root.findViewById(R.id.recycler_view);
        CategorieParentAdapter adapter = new CategorieParentAdapter(cats,requireContext());
        System.out.println("adapteeer:" +adapter.getItemCount());
        recycler.setAdapter(adapter);
    }

    public void retrieveData() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
//            GsonRequest<CategorieResponse[]> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_CATEGORIE_BY_PARENT+"/"+idparent+"?allchild=true"), headers, null, CategorieResponse[].class,
//                    HomeFragment.this::onResponse, HomeFragment.this::onFaild);
            GsonRequest<ProgrammeResponse[]> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_PROGRAMME), headers, null, ProgrammeResponse[].class,
                    HomeFragment.this::onResponse, Throwable::printStackTrace);
            request.setContentType("application/json");
            queue.add(request);
        });
    }

    public  <T> long onResponseCat(Categorie t) {
        return t.getIdParent();
//        HomeFragment fragment = new HomeFragment();
//        Bundle bundle = new Bundle();
//        bundle.putLong("idparrain",t.getIdParent());
//        bundle.putLong("idparrain",t.getIdParent());
//        fragment.setArguments(bundle);
    }

    private void onFaild(VolleyError volleyError) {
    }

    private <T> void onResponse(ProgrammeResponse[] desc) {
        for (int i = 0; i < desc.length; i++) {
            System.out.println(desc.length);
            createGridProgramme(desc);
        }
    }
}