package mbds.tpt.pari.services.payload.response;

import java.time.LocalDateTime;
import mbds.tpt.pari.models.Etat;

public class ProgrammeResponse {
    private Object createdAt;
    private Object updatedAt;
    private Object updatedBy;
    private long id;
    private String dateHeurePgm;
    private String libelle;
    private Etat etat;

    public ProgrammeResponse(Object createdAt, Object createdBy, Object updatedBy, long id, String dateHeurePgm, String libelle, Etat etat) {
        this.createdAt = createdAt;
        this.updatedAt = createdBy;
        this.updatedBy = updatedBy;
        this.id = id;
        this.dateHeurePgm = dateHeurePgm;
        this.libelle = libelle;
        this.etat = etat;
    }

    public ProgrammeResponse() {
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getCreatedBy() {
        return updatedAt;
    }

    public void setCreatedBy(Object createdBy) {
        this.updatedAt = createdBy;
    }

    public Object getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Object updatedBy) {
        this.updatedBy = updatedBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDateHeurePgm() {
        return dateHeurePgm;
    }

    public void setDateHeurePgm(String dateHeurePgm) {
        this.dateHeurePgm = dateHeurePgm;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Object getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    @Override
    public String toString() {
        return "ProgrammeResponse{" +
                "createdAt=" + createdAt +
                ", createdBy=" + updatedAt +
                ", updatedBy=" + updatedBy +
                ", id=" + id +
                ", dateHeurePgm=" + dateHeurePgm +
                ", libelle='" + libelle + '\'' +
                ", etat=" + etat +
                '}';
    }
}

