package mbds.tpt.pari.ui.scann;

import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mbds.tpt.pari.R;
import mbds.tpt.pari.services.UssdService;

import static android.content.Context.MODE_PRIVATE;
import static android.provider.Settings.Secure.ACCESSIBILITY_ENABLED;
import static android.provider.Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES;
import static android.provider.Settings.Secure.getInt;

public class ScannFragment extends Fragment {

    private ScannViewModel mViewModel;
    private static final int REQUEST_PERMISSIONS = 201;

    public static ScannFragment newInstance() {
        return new ScannFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scann_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ScannViewModel.class);
        // TODO: Use the ViewModel
        setRequestPermissions();
    }
    private boolean isAccessibilitySettingsOn(Context mContext) throws Settings.SettingNotFoundException {
        int                  accessibilityEnabled = getInt(mContext.getApplicationContext().getContentResolver(), ACCESSIBILITY_ENABLED);
        final String         service              = String.format("%s/%s", mContext.getPackageName(), UssdService.class.getCanonicalName());
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(mContext.getApplicationContext().getContentResolver(), ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    private void setRequestPermissions() {
        try {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.BIND_ACCESSIBILITY_SERVICE}, REQUEST_PERMISSIONS);
            if (!isAccessibilitySettingsOn(requireContext())) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }

}