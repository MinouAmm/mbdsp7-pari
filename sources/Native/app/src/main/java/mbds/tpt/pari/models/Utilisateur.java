package mbds.tpt.pari.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.sql.Date;
import java.util.List;

import mbds.tpt.pari.converter.JsonConverters;

@Entity(tableName = "users")
@TypeConverters(value = JsonConverters.class)
public class Utilisateur {

    @PrimaryKey
    private Long id;
    private Utilisateur idParrain;
    private String email;
    private String username;
    private String dateNaissance;
    private String password;
    private String codeParrain;
    private double solde;
    private String roles;
    public Utilisateur(String user, String password) {
        username = user;
        this.password = password;
    }

    public Utilisateur() {
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Utilisateur(Long id, String email, String username, String roles) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.roles = roles;
    }

    public Utilisateur(Utilisateur idParrain, String email, String username, String dateNaissance, String password, String codeParrain, double solde) {
        this.idParrain = idParrain;
        this.email = email;
        this.username = username;
        this.dateNaissance = dateNaissance;
        this.password = password;
        this.codeParrain = codeParrain;
        this.solde = solde;
    }
    public Utilisateur(String email, String username, String dateNaissance, String password, String codeParrain, double solde,String roles) {
        this.email = email;
        this.username = username;
        this.dateNaissance = dateNaissance;
        this.password = password;
        this.codeParrain = codeParrain;
        this.solde = solde;
        this.roles = roles;
    }

    public Utilisateur(Long id, Utilisateur idParrain, String email, String username, String dateNaissance, String password, String codeParrain, double solde) {
        this.id = id;
        this.idParrain = idParrain;
        this.email = email;
        this.username = username;
        this.dateNaissance = dateNaissance;
        this.password = password;
        this.codeParrain = codeParrain;
        this.solde = solde;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Utilisateur getIdParrain() {
        return idParrain;
    }

    public void setIdParrain(Utilisateur idParrain) {
        this.idParrain = idParrain;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCodeParrain() {
        return codeParrain;
    }

    public void setCodeParrain(String codeParrain) {
        this.codeParrain = codeParrain;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
/*public boolean isValide() {
        /*TODO:REST api for login*/
        /*if ("client".equals(pseudo) && "client".equals(mdp))
            return true;
        else
            return false;
    }*/
}
