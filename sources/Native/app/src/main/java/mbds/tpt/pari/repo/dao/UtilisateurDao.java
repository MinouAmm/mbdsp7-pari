package mbds.tpt.pari.repo.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import mbds.tpt.pari.models.Utilisateur;

@Dao
public interface UtilisateurDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(Utilisateur user);

    @Delete
    void delete(Utilisateur user);

    @Query("select * from users limit 1")
    LiveData<Utilisateur> get();
}
