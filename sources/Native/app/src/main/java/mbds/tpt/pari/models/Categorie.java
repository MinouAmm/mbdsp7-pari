package mbds.tpt.pari.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import mbds.tpt.pari.converter.JsonConverters;

@Entity(tableName = "Categorie")
@TypeConverters(value = JsonConverters.class)
public class Categorie {
    @PrimaryKey
    private long id;
    private long idParent;
    private  String nomCateg;
    private int rang;
    private boolean isSelected;

    public Categorie() {
    }

    public Categorie(long id,long idParent, String nom_categ, int rang) {
        this.id = id;
        this.idParent = idParent;
        this.nomCateg = nom_categ;
        this.rang = rang;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdParent() {
        return idParent;
    }

    public void setIdParent(long idParent) {
        this.idParent = idParent;
    }

    public String getNomCateg() {
        return nomCateg;
    }

    public void setNomCateg(String nom_categ) {
        this.nomCateg = nom_categ;
    }

    public int getRang() {
        return rang;
    }

    public void setRang(int rang) {
        this.rang = rang;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
    @Override
    public String toString() {
        return "Categorie{" +
                "id=" + id +
                ", nom_categ='" + nomCateg + '\'' +
                '}';
    }
}
