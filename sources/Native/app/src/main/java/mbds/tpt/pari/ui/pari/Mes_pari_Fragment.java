package mbds.tpt.pari.ui.pari;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mbds.tpt.pari.R;

public class Mes_pari_Fragment extends Fragment {

    private MesPariViewModel mViewModel;

    public static Mes_pari_Fragment newInstance() {
        return new Mes_pari_Fragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mes_pari__fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MesPariViewModel.class);
        // TODO: Use the ViewModel
    }

}