package mbds.tpt.pari.constants;

import mbds.tpt.pari.R;

public final class Nav {
    public static final int[] NAVS_ID = {
            R.id.nav_home,
            R.id.nav_pari,
            R.id.nav_profil,
            R.id.nav_descendant
    };
}
