package mbds.tpt.pari.adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import mbds.tpt.pari.R;
import mbds.tpt.pari.databinding.CardCategorieBinding;
import mbds.tpt.pari.holder.CategorieHolder;
import mbds.tpt.pari.services.payload.response.CategorieResponse;
import mbds.tpt.pari.ui.DetailProgrammeActivity;
import mbds.tpt.pari.ui.categorie.CategorieFragment;
import mbds.tpt.pari.ui.programme.ProgrammeFragment;

public class CategorieAdapter extends RecyclerView.Adapter<CategorieHolder> {
    private CategorieResponse[] cats;
    private Context context;

    public CategorieAdapter(CategorieResponse[] providers,Context context) {
        this.cats = providers;
        this.context = context;
    }

    @NonNull
    @Override
    public CategorieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater       inflater     = LayoutInflater.from(parent.getContext());
        CardCategorieBinding categBinding = DataBindingUtil.inflate(inflater, R.layout.card_categorie, parent, false);
        return new CategorieHolder(categBinding, categBinding.cardCategorie);
    }

    @Override
    public void onBindViewHolder(CategorieHolder categHolder, int position) {
        CategorieResponse catg = cats[position];
        categHolder.getEventBinding().setCategorie(catg);
        categHolder.getFav().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int count =0;
                count++;
                System.out.println(count);
                favorit(categHolder);
            }
        });
        categHolder.getEventBinding().cardCategorie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailProgrammeActivity.class);
                for (int i = 0; i < cats.length; i++) {
                    System.out.println("id: "+cats[i].getId());
                    intent.putExtra("id", cats[i].getId());
                }
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cats.length;
    }
    private void favorit(CategorieHolder categHolder) {
        categHolder.getFav().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)  {
                    categHolder.getFav().setChecked(true);
                } else {
                 categHolder.getFav().setChecked(false);}
                }
            });
    }
    /*private void showResult()  {
        String message = null;
        if(this.categFav.isChecked()) {
            message =  this.categFav.getText().toString();
        }
        message = message == null? "You select nothing": "Ta favorie: " + message;
        Toast.makeText(CategorieFragment.newInstance().getContext(), message, Toast.LENGTH_LONG).show();
    }*/

    // When "Check" change state.
//    private void checkChange(boolean isChecked)  {
//        this.categFav.setChecked(isChecked);
//    }

}
