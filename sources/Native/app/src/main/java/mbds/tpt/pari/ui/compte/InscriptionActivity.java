package mbds.tpt.pari.ui.compte;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.MainActivity;
import mbds.tpt.pari.R;
import mbds.tpt.pari.constants.PathApi;
import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.models.Role;
import mbds.tpt.pari.models.Utilisateur;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.JwtResponse;

import static mbds.tpt.pari.constants.Util.GSON;
import static mbds.tpt.pari.constants.Util.TOKEN;
import static mbds.tpt.pari.constants.Util.USERS_ID;


public class InscriptionActivity extends AppCompatActivity {
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
    private UtilisateurViewModel viewModel;
    private Date currDate = new Date();
    private SharedPreferences shared;
    String userID, bearer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        shared = getSharedPreferences(Util.SHARED_SESSION, MODE_PRIVATE);
        userID = shared.getString(USERS_ID, null);
        bearer = shared.getString(TOKEN, null);
        onClickRegister();
    }

    public void onClickRegister() {
        EditText pseudo = findViewById(R.id.pseudo);
        EditText email = findViewById(R.id.email);
        EditText codeparrain = findViewById(R.id.code);
        EditText motdepasse = findViewById(R.id.motdepasse);
        EditText datenaissance = findViewById(R.id.datenaissance);

        final Button button = findViewById(R.id.btn_inscrire);
        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(pseudo.getText()) && !TextUtils.isEmpty(motdepasse.getText())) {
                    progres(true);
                    String pseu = pseudo.getText().toString();
                    String mail = email.getText().toString();
                    //vide raha tsisy code
                    String code = codeparrain.getText().toString();
                    String password = motdepasse.getText().toString();
                    String birth = datenaissance.getText().toString();
                    String role ="ROLE_USER" ;
                    double solde = 0;
                    final Utilisateur inscrire = new Utilisateur(mail,pseu,birth,password,code,solde,role);
                    /*Appel REST API*/
                    EXECUTOR_SERVICE.execute(() -> {
                        RequestQueue queue = Volley.newRequestQueue(InscriptionActivity.this);
                        GsonRequest<JwtResponse> request = new GsonRequest<>(Request.Method.POST, Url.path(PathApi.REGISTER), null, GSON.toJson(inscrire), JwtResponse.class,
                                InscriptionActivity.this::onResponse, InscriptionActivity.this::onFaild);
                        request.setContentType("application/json");
                        queue.add(request);
                    });
                } else {
                    Toast.makeText(InscriptionActivity.this, "Invalid nom d'utilisateur ou mots de passe", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private void progres(boolean show) {
        ProgressBar bar = findViewById(R.id.loading_spinner);
        bar.setVisibility(show ? View.VISIBLE : View.GONE);
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (userID != null && bearer != null) {
            Intent i = new Intent(InscriptionActivity.this, MainActivity.class);
            startActivity(i);
        }
    }

    private void onResponse(JwtResponse response) {
        SharedPreferences.Editor editor = shared.edit();
        System.out.println("----------------" + response.toString());

        // below two lines will put values for
        // email and username, ... in shared preferences.
        editor.putString(USERS_ID, response.getId() + "");
        editor.putString(TOKEN, response.getAccessToken());
        editor.putString("email", response.getEmail());
        editor.putString("username", response.getUsername());
        editor.putString("roles", String.valueOf(response.getRoles()));
        editor.putString("tokenType", String.valueOf(response.getTokenType()));
        editor.putString("accessToken", String.valueOf(response.getAccessToken()));

        // to save our data with key and value.
        editor.apply();

        // starting new activity.
        onStart();
    }

    private void onFaild(VolleyError volleyError) {
        String errorMessage;
        if (volleyError != null) {
            if (volleyError.toString().equalsIgnoreCase("com.android.volley.TimeoutError")) {
                errorMessage = "Délais de connexion dépassé, veuillez réessayez plus tard";
                Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
            } else {
                int status = volleyError.networkResponse.statusCode;
                System.out.println(status);
                if (status == 401) {
                    errorMessage = "Nom d' utilisateur ou mots de passe invalide";
                    Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
                } else {
                    errorMessage = " Bienvenue.";
                    Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            errorMessage = "Une erreur s'est produite lors du connexion, veuillez réessayez plus tard.";
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
        progres(false);
        System.out.println(errorMessage);
    }
}