package mbds.tpt.pari;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.content.ContextCompat;

import com.cloudinary.android.MediaManager;

import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.ui.compte.LoginActivity;

public class SplashscreenActivity extends AppCompatActivity {
    private SharedPreferences shared;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorSplashScreen_Start));
        }
        setContentView(R.layout.activity_splashscreen);
        checkLogin();
        /*handler=new Handler();
        handler.postDelayed(() -> {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }, 3000);*/
    }

    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    private void checkLogin() {
        Handler handler = new Handler();
        shared = getSharedPreferences(Util.SHARED_SESSION, MODE_PRIVATE);
        boolean     isConnected = shared.getBoolean(Util.USER_CONNECTED, false);
        setMediaManager();

        if (isConnected) {
            handler.postDelayed(() -> {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            }, 500);
        } else {
            handler.postDelayed(() -> {
                Intent intent = new Intent(SplashscreenActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }, 2000);
        }
    }

    private void setMediaManager() {
        try {
            MediaManager.init(this);
        } catch (IllegalStateException ignored) {
        }
    }
}