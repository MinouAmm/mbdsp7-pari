package mbds.tpt.pari.repo;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import mbds.tpt.pari.models.Categorie;
import mbds.tpt.pari.repo.dao.CategorieDao;

public class CategorieRepo {
    private CategorieDao CategorieDao;
    private AppDatabase     _db;
    private LiveData<List<Categorie>> Categories;

    public CategorieRepo(Application application) {
        _db = AppDatabase.getDatabase(application);
        CategorieDao = _db.CategorieDao();
        Categories = CategorieDao.getAll();
    }

    public void inserts(Categorie... Categories) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            CategorieDao.insertAll(Categories);
        });
    }

    public LiveData<List<Categorie>> getCategories() {
        return Categories;
    }

    public LiveData<Categorie> getCategorie(long CategorieId) {
        return CategorieDao.getById(CategorieId);
    }

    public LiveData<List<Categorie>> getCategorieByIds(Long[] ids) {
        return CategorieDao.findByIds(ids);
    }
}
