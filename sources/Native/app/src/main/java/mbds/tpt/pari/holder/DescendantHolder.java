package mbds.tpt.pari.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mbds.tpt.pari.databinding.CardCategorieBinding;
import mbds.tpt.pari.databinding.CardDescendantBinding;

public class DescendantHolder extends RecyclerView.ViewHolder {
private CardDescendantBinding categBinding;

public DescendantHolder(CardDescendantBinding eventBinding, @NonNull View itemView) {
        super(itemView);
        this.categBinding = eventBinding;
        }

public CardDescendantBinding getEventBinding() { return categBinding; }
}
