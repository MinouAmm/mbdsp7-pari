package mbds.tpt.pari.models;

import androidx.room.TypeConverters;

import mbds.tpt.pari.converter.JsonConverters;

@TypeConverters(value = JsonConverters.class)
public class Equipe {
    private Object createdBy;
    private long id;
    private String nomEquipe;
    private String coach;
    public Equipe() {
    }

    public Equipe(long id, String nomEquipe, String coach) {
        this.id = id;
        this.nomEquipe = nomEquipe;
        this.coach = coach;
    }

    public Equipe(Object createdBy, long id_equipe, String nom_equipe, String coach) {
        this.createdBy = createdBy;
        this.id = id_equipe;
        this.nomEquipe = nom_equipe;
        this.coach = coach;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomEquipe() {
        return nomEquipe;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }
}
