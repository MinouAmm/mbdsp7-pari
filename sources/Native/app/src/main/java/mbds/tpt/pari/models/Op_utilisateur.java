package mbds.tpt.pari.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.sql.Date;

import mbds.tpt.pari.converter.JsonConverters;


@Entity(tableName = "op_utilisateur")
@TypeConverters(value = JsonConverters.class)
public class Op_utilisateur {

        @PrimaryKey
        private long id;
        private String pseudo;
        private String motdepasse;
        private long montant;
        private long solde;
        private Date date_op;

}