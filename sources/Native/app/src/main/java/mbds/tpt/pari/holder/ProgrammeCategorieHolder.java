package mbds.tpt.pari.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mbds.tpt.pari.databinding.CardHomeBinding;
import mbds.tpt.pari.databinding.CardProgrammeBinding;

public class ProgrammeCategorieHolder extends RecyclerView.ViewHolder {
private CardProgrammeBinding categBinding;

public ProgrammeCategorieHolder(CardProgrammeBinding eventBinding, @NonNull View itemView) {
        super(itemView);
        this.categBinding = eventBinding;
        }

public CardProgrammeBinding getEventBinding() { return categBinding; }
}
