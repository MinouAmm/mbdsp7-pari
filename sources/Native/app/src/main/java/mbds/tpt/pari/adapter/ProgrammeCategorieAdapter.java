package mbds.tpt.pari.adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import mbds.tpt.pari.R;
import mbds.tpt.pari.databinding.CardProgrammeBinding;
import mbds.tpt.pari.holder.ProgrammeCategorieHolder;
import mbds.tpt.pari.services.payload.response.CoteByPgmResponse;
import mbds.tpt.pari.services.payload.response.ProgrammeCategorieResponse;
import mbds.tpt.pari.ui.detailpari.DetailFragment;

public class ProgrammeCategorieAdapter extends RecyclerView.Adapter<ProgrammeCategorieHolder> {
    private ProgrammeCategorieResponse[] pgrC;
     CoteByPgmResponse cote;
     Context context;
    CardProgrammeBinding categBinding;
    public ProgrammeCategorieAdapter(ProgrammeCategorieResponse[] providers, Context context) {
        this.pgrC = providers;
        this.context = context;
    }
    public ProgrammeCategorieAdapter(CoteByPgmResponse cote, Context context) {
        this.cote = cote;
        this.context = context;
    }

    @NonNull
    @Override
    public ProgrammeCategorieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater       inflater     = LayoutInflater.from(parent.getContext());
        categBinding = DataBindingUtil.inflate(inflater, R.layout.card_programme, parent, false);
        return new ProgrammeCategorieHolder(categBinding, categBinding.cardProgramme);
    }

    @Override
    public void onBindViewHolder(ProgrammeCategorieHolder categHolder, int position) {
        ProgrammeCategorieResponse catg = pgrC[position];
        System.out.println("taille pgrC :"+pgrC.length);
        categHolder.getEventBinding().setProgramme(catg);
//        categBinding.cardProgramme.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.O)
//            @Override
//            public void onClick(View v) {
//                categBinding.cardProgramme.setTag(position);
//                int posit = (int)categBinding.cardProgramme.getTag();
//                Toast.makeText(context, String.valueOf(pgrC[posit]), Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(context, DetailFragment.class);
//                System.out.println("id:::: "+pgrC[posit].getId());
//                intent.putExtra("id", pgrC[posit].getId());
//                context.startActivity(intent);
//            }
//        });
       /* CoteByPgmResponse coteP = cote[position];
        categHolder.getEventBinding().setCote(coteP);
        System.out.println("taille cotee :"+cote.length);*/
    }

    @Override
    public int getItemCount() {
        return pgrC.length;
    }
}
