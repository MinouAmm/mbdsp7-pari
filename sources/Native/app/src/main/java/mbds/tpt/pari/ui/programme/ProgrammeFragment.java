package mbds.tpt.pari.ui.programme;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.internal.LinkedTreeMap;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.R;
import mbds.tpt.pari.adapter.DescendantAdapter;
import mbds.tpt.pari.adapter.GridItemDecoration;
import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.models.Equipe;
import mbds.tpt.pari.models.Programme;
import mbds.tpt.pari.placeholder.PlaceholderContent;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.CoteByPgmResponse;
import mbds.tpt.pari.services.payload.response.DescendantsResponse;
import mbds.tpt.pari.services.payload.response.ProgrammeCategorieResponse;
import mbds.tpt.pari.ui.DetailProgrammeActivity;
import mbds.tpt.pari.ui.detailpari.DetailFragment;

import static mbds.tpt.pari.MainActivity.bearer;
import static mbds.tpt.pari.constants.PathApi.GET_COTE_BY_PGM;
import static mbds.tpt.pari.constants.PathApi.GET_PROGRAMME_BY_CATEGORIE;

/**
 * A fragment representing a list of Items.
 */
public class ProgrammeFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    long idpgm = 0;
    long idCateg = 0;
    View view;
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProgrammeFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ProgrammeFragment newInstance(int columnCount) {
        ProgrammeFragment fragment = new ProgrammeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            idCateg =getArguments().getLong("id");
            idpgm =getArguments().getLong("id");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_programme, container, false);
        RecyclerView recycler = view.findViewById(R.id.recycler_view);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false));
        int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        recycler.addItemDecoration(new GridItemDecoration(largePadding, smallPadding));
        // Set the adapter
//        if (view instanceof RecyclerView) {
//            Context context = view.getContext();
//            RecyclerView recyclerView = (RecyclerView) view;
//            if (mColumnCount <= 1) {
//                recyclerView.setLayoutManager(new LinearLayoutManager(context));
//            } else {
//                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
//                retrieveData();
//                retrieveDataCote();
//            }
//            recyclerView.setAdapter(new ProgrammeRecyclerViewAdapter(PlaceholderContent.ITEMS));
            retrieveData();
            retrieveDataCote();

//        }
        return view;
    }
    private void retrieveData() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<ProgrammeCategorieResponse> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_PROGRAMME_BY_CATEGORIE + "/" + idCateg + "/programmes?withEquipes=true"), headers, null, ProgrammeCategorieResponse.class,
                    ProgrammeFragment.this::onResponse, this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }

    private void onFaild(VolleyError volleyError) {
        System.out.println(volleyError.getMessage());
        System.out.println(volleyError);
    }

//    @RequiresApi(api = Build.VERSION_CODES.O)
//    private <T> void onResponse(ProgrammeCategorieResponse desc) {
////
//        List<Programme> programmes = null;
//        List<Equipe> lstequipes = new ArrayList<>();
//        Programme programme = new Programme();
//        Equipe equipe = new Equipe();
//        long idcat = desc.getId();
//        for (Object object : desc.getProgrammes()) {
//            if (programmes == null) programmes = new ArrayList<>();
//            LinkedTreeMap linkedTreeMap = (LinkedTreeMap) object;
//            Date createdAt = getDateFromString((String) linkedTreeMap.get("createdAt"), "yyyy-MM-dd'T'hh:mm:ss");
//            Object createdBy = linkedTreeMap.get("createdBy");
//            idpgm= ((Double) linkedTreeMap.get("id")).longValue();
//            String str = (String) linkedTreeMap.get("dateHeurePgm");
//            Date datePg = getDateFromString(str, "yyyy-MM-dd'T'hh:mm:ss");
//            String libelle = (String) linkedTreeMap.get("libelle");
//            List<Equipe> equipes = (List<Equipe>) linkedTreeMap.get("equipes");
//            long idequipe = 0;String nomEquipe = "",coach = "";
//            for (Object eqp : equipes) {
//                LinkedTreeMap resp = (LinkedTreeMap) eqp;
//                LinkedTreeMap equipeeeee = (LinkedTreeMap) resp.get("equipe");
//                idequipe = ((Double) equipeeeee.get("id")).longValue();
//                nomEquipe = (String) equipeeeee.get("nomEquipe");
//                coach = (String) equipeeeee.get("coach");
//            }
//            programme = new Programme(createdAt, createdBy, idpgm, datePg.toInstant().atZone(ZoneId.systemDefault())
//                    .toLocalDateTime(), libelle, equipes);
//            equipe = new Equipe(idequipe,nomEquipe,coach);
//            programmes.add(programme);
//            lstequipes.add(equipe);
//        }
////        setContentView(R.layout.card_programme);
//        LinearLayout linearEquipe = view.findViewById(R.id.linearEquipe);
//        LinearLayout linearCateg = view.findViewById(R.id.linearCateg);
//        LinearLayout linearLayout = view.findViewById(R.id.linear);
//        LinearLayout lineardate = view.findViewById(R.id.lineardate);
//        TextView catg = view.findViewById(R.id.nomCateg);
//        catg.setText(desc.getNomCateg());
//        linearCateg.removeAllViews();
//        linearCateg.addView(catg);
//        TextView lstpgm = null;
//        for (int i = 0; i < Objects.requireNonNull(programmes).size(); i++) {
//            lstpgm = new TextView(requireContext());
//            TextView datePgm = new TextView(requireContext());
//            TextView equip = new TextView(requireContext());
//            equip.setText(lstequipes.get(i).getNomEquipe());
//            linearEquipe.addView(equip);
//            lstpgm.setText(programmes.get(i).getLibelle());
//            datePgm.setText(String.valueOf(programmes.get(i).getDateHeurePgm()));
//            linearLayout.addView(lstpgm);
//            lineardate.addView(datePgm);
//        }
//        final DetailFragment myFragment = new DetailFragment();
//        FragmentManager fragmentManager = ((AppCompatActivity)requireContext()).getSupportFragmentManager();
//        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        assert lstpgm != null;
//        lstpgm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bundle b = new Bundle();
//                System.out.println("idpgmmm: "+idpgm);
//                b.putLong("id", idpgm);
//                myFragment.setArguments(b);
////                fragmentTransaction.show(myFragment);
//                fragmentTransaction.replace(R.id.card_programme,myFragment).commit();
//                //fragmentTransaction.add(R.id.card_programme, myFragment)
//            }
//        });
//    }
private <T> void onResponse(ProgrammeCategorieResponse desc) {
//    for (int i = 0; i < desc.length; i++) {
//        System.out.println(desc.length);
//        createGridProgramme(desc);
//    }
}
    private void createGridProgramme(DescendantsResponse[] cats) {
        RecyclerView recycler = view.findViewById(R.id.recycler_view);
        DescendantAdapter adapter = new DescendantAdapter(cats);
        System.out.println("adapteeerDesc:" +adapter.getItemCount());
        recycler.setAdapter(adapter);
    }

    private void retrieveDataCote() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<CoteByPgmResponse> request = new GsonRequest<>(Request.Method.GET, Url.pathURL(GET_COTE_BY_PGM + "/" + idpgm), headers, null, CoteByPgmResponse.class,
                    ProgrammeFragment.this::onResponseCote, this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }
    private <T> void onResponseCote(CoteByPgmResponse desc) {
        System.out.println("cotee :" +desc.toString());

    }
    private Date getDateFromString(String dateS, String format) {
        Date date = null;
        try {
            date = new SimpleDateFormat(format).parse(dateS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }
}