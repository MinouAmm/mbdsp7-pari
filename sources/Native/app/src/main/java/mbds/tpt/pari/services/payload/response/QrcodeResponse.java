package mbds.tpt.pari.services.payload.response;

import android.graphics.Bitmap;

import androidx.room.TypeConverters;

import mbds.tpt.pari.converter.JsonConverters;

public class QrcodeResponse {
    @TypeConverters(value = JsonConverters.class)
    Bitmap PNG;

    public QrcodeResponse() {
    }

    public QrcodeResponse(Bitmap bufferedImage) {
        this.PNG = bufferedImage;
    }

    public Bitmap getPNG() {
        return PNG;
    }

    public void setPNG(Bitmap bufferedImage) {
        this.PNG = bufferedImage;
    }
}
