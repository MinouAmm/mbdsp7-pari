package mbds.tpt.pari.services.payload.response;

import java.util.Date;

import mbds.tpt.pari.models.Parrain;

public class DescendantsResponse {
    private long id ;
    private Parrain parrain;
    private String username ;
    private  String email;
    private Date dateNaissance;
    private Double solde;
    private String codeParrain;

    public DescendantsResponse() {
    }

    public DescendantsResponse(long id, Parrain parrain, String username, String email, Date dateNaissance, Double solde, String codeParrain) {
        this.id = id;
        this.parrain = parrain;
        this.username = username;
        this.email = email;
        this.dateNaissance = dateNaissance;
        this.solde = solde;
        this.codeParrain = codeParrain;
    }

    public Parrain getParrain() {
        return parrain;
    }

    public void setParrain(Parrain parrain) {
        this.parrain = parrain;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public String getCodeParrain() {
        return codeParrain;
    }

    public void setCodeParrain(String codeParrain) {
        this.codeParrain = codeParrain;
    }
}
