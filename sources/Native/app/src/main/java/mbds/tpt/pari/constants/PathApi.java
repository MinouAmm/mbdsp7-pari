package mbds.tpt.pari.constants;

import static mbds.tpt.pari.MainActivity.userID;

public class PathApi {
    public static final String LOGIN       = "/api/auth/signin";
    public static final String REGISTER       = "/api/auth/signup";
    // Get List categorie
    public static final String GET_CATEGORIE = "/api/categories";
    // Get List categorie_BY_PARENT
    public static final String GET_CATEGORIE_BY_PARENT = "/api/categories/parent";
    // Get categorie by id
    public static final String GET_CATEGORIE_BY_ID = "/api/categories/{id}";
    // Get List users
    public static final String GET_USERS = "/api/users";
    // Get user by id
    public static final String GET_USER_BY_ID = "/api/users";
    // Get equipe by id
    public static final String GET_EQUIPE_BY_ID = "/api/equipes/{idequip}";
    // Get programmes by categorie
    public static final String GET_PROGRAMME_BY_CATEGORIE ="/api/categories";
    // Get pgm
    public static final String GET_PROGRAMME="/api/programmes";
    // Get pgm by id
    public static final String GET_PROGRAMME_BY_ID="/api/programmes";
    // Get user qrcode by id
    public static final String GET_QRCODE_BY_ID = "/api/users";
    // Favori
    public static final String PUT_FAVORI="/api/users";
    // Find user child
    public static final String GET_DESCENDANT = "/api/users/parrain";

    //cote findByProgramme
    public static final String GET_COTE_BY_PGM = "/api/cotes/programme";

    //add pari
    public static final String POST_PARI ="/api/paris";

}
