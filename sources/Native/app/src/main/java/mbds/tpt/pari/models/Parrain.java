package mbds.tpt.pari.models;

public class Parrain {
    private long id;

    public Parrain() {
    }

    public Parrain(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
