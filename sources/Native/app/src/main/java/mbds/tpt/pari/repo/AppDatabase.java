package mbds.tpt.pari.repo;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.models.Categorie;
import mbds.tpt.pari.models.Op_utilisateur;
import mbds.tpt.pari.models.Utilisateur;
import mbds.tpt.pari.repo.dao.CategorieDao;
import mbds.tpt.pari.repo.dao.UtilisateurDao;

@Database(entities = {Utilisateur.class,Categorie.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UtilisateurDao utilisateurDao();
    public abstract CategorieDao CategorieDao();

    private static volatile AppDatabase     INSTANCE;
    private static final    int             NUMBER_OF_THREADS     = 2;
    static final            ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "pari_db")
                            .addCallback(databaseInitCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    static RoomDatabase.Callback databaseInitCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            databaseWriteExecutor.execute(() -> {
                //insertion dans db
            });
        }
    };
}