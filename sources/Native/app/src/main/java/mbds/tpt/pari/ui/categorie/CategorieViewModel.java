package mbds.tpt.pari.ui.categorie;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import mbds.tpt.pari.models.Categorie;
import mbds.tpt.pari.repo.CategorieRepo;

public class CategorieViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private CategorieRepo CategorieRepo;
    private LiveData<List<Categorie>> categories;

    public CategorieViewModel(Application application) {
        super();
        CategorieRepo = new CategorieRepo(application);
        categories = CategorieRepo.getCategories();
    }

    public LiveData<List<Categorie>> getCategories() {
        return categories;
    }

    public void inserts(Categorie... categories) {
        CategorieRepo.inserts(categories);
    }

    public LiveData<Categorie> getCategorie(long CategorieId) {
        return CategorieRepo.getCategorie(CategorieId);
    }

    public LiveData<List<Categorie>> getCategorieByIds(Long... ids) {
        return CategorieRepo.getCategorieByIds(ids);
    }
}