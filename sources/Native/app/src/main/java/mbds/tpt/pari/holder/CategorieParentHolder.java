package mbds.tpt.pari.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mbds.tpt.pari.databinding.CardHomeBinding;

public class CategorieParentHolder extends RecyclerView.ViewHolder {
private CardHomeBinding categBinding;

public CategorieParentHolder(CardHomeBinding eventBinding, @NonNull View itemView) {
        super(itemView);
        this.categBinding = eventBinding;
        }

public CardHomeBinding getEventBinding() { return categBinding; }
}
