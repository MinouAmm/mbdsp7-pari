package mbds.tpt.pari.converter;

import androidx.room.TypeConverter;

import java.sql.Date;


import mbds.tpt.pari.models.Op_utilisateur;
import mbds.tpt.pari.models.Utilisateur;

import static mbds.tpt.pari.constants.Util.GSON;
import static mbds.tpt.pari.constants.Util.hasValue;


public class JsonConverters {
    @TypeConverter
    public Op_utilisateur toOject(String value) {
        return value == null ? null : GSON.fromJson(value, Op_utilisateur.class);
    }

    @TypeConverter
    public String toJson(Op_utilisateur info) {
        return info == null ? null : GSON.toJson(info);
    }

    @TypeConverter
    public Date getDate(String value) {
        return hasValue(value) ? GSON.fromJson(value, Date.class) : null;
    }

    @TypeConverter
    public String setDate(Date value) {
        return hasValue(value) ? GSON.toJson(value) : null;
    }

    @TypeConverter
    public Utilisateur getUtilisateur(String value) {
        return hasValue(value) ? GSON.fromJson(value, Utilisateur.class) : null;
    }

    @TypeConverter
    public String setUtilisateur(Utilisateur user) {
        return hasValue(user) ? GSON.toJson(user) : null;
    }
}