package mbds.tpt.pari.adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;

import mbds.tpt.pari.R;
import mbds.tpt.pari.databinding.CardHomeBinding;
import mbds.tpt.pari.holder.CategorieParentHolder;
import mbds.tpt.pari.services.payload.response.CategorieResponse;
import mbds.tpt.pari.services.payload.response.ProgrammeResponse;
import mbds.tpt.pari.ui.DetailProgrammeActivity;
import mbds.tpt.pari.ui.detailpari.DetailFragment;
import mbds.tpt.pari.ui.home.HomeFragment;

public class CategorieParentAdapter extends RecyclerView.Adapter<CategorieParentHolder> {
    //private List<Categorie> pgms;
    private ProgrammeResponse[] pgms;
    private Context context;
    CardHomeBinding categBinding;
    public CategorieParentAdapter(ProgrammeResponse[] providers, Context context) {
        this.pgms = providers;
        this.context = context;
    }

    @NonNull
    @Override
    public CategorieParentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater       inflater     = LayoutInflater.from(parent.getContext());
        categBinding = DataBindingUtil.inflate(inflater, R.layout.card_home, parent, false);
        return new CategorieParentHolder(categBinding, categBinding.cardHome);
    }

    @Override
    public void onBindViewHolder(CategorieParentHolder categHolder, int position) {
        ProgrammeResponse catg = pgms[position];
        categHolder.getEventBinding().setCategorie(catg);
        HomeFragment myFragment = new HomeFragment();
        DetailFragment detailFragment = new DetailFragment();
            categBinding.cardHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categBinding.cardHome.setTag(position);
                int posit = (int)categBinding.cardHome.getTag();
                long id = pgms[posit].getId();
//                long idparent = pgms[posit].getParent().get;
                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                Bundle b = new Bundle();
                System.out.println("idpgmHomeee: "+id);
                b.putLong("id", id);
                detailFragment.setArguments(b);
                fragmentTransaction.remove(myFragment);
                fragmentTransaction.show(detailFragment);
                fragmentTransaction.replace(R.id.card_home,detailFragment);
                fragmentTransaction.addToBackStack(null).commit();

//                if (idparent!=0) {
//                    Intent intent = new Intent(context, DetailFragment.class);
//                    System.out.println("id:::: " + id);
//                    intent.putExtra("id", id);
//                    context.startActivity(intent);
//                } else {
//                    homeFragment.retrieveData(idparent,context);
//                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return pgms.length;
    }
}
