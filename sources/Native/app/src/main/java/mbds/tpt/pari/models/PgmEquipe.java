package mbds.tpt.pari.models;

public class PgmEquipe {
    private Object id;
    private Object equipe;
    private int rang;

    public PgmEquipe() {
    }

    public PgmEquipe(Id id, Equipe equipe, int rang) {
        this.id = id;
        this.equipe = equipe;
        this.rang = rang;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getEquipe() {
        return equipe;
    }

    public void setEquipe(Object equipe) {
        this.equipe = equipe;
    }

    public int getRang() {
        return rang;
    }

    public void setRang(int rang) {
        this.rang = rang;
    }
}
