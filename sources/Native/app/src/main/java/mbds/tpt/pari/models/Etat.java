package mbds.tpt.pari.models;

public class Etat {
    private Object createdBy;
    private long id;
    private String libelle;
    public Etat() {
    }

    public Etat(Long id_etat, String libelle) {
        this.id = id_etat;
        this.libelle = libelle;
    }

    public Long getId_etat() {
        return id;
    }

    public void setId_etat(Long id_etat) {
        this.id = id_etat;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
