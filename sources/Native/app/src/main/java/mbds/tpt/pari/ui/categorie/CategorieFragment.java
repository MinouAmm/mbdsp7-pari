package mbds.tpt.pari.ui.categorie;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.R;
import mbds.tpt.pari.adapter.CategorieAdapter;
import mbds.tpt.pari.adapter.GridItemDecoration;
import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.CategorieResponse;

import static mbds.tpt.pari.MainActivity.bearer;
import static mbds.tpt.pari.constants.PathApi.GET_CATEGORIE;
import static mbds.tpt.pari.constants.PathApi.PUT_FAVORI;

public class CategorieFragment extends Fragment {

    private CategorieViewModel mViewModel;
    private static CategorieResponse response;
    public static String ACCESS_TOKEN;
    private ViewModelProvider viewModelProvider;
    private CheckBox categFav;
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);

    public static CategorieFragment newInstance() {
        return new CategorieFragment();
    }
    View root;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.categorie_fragment, container, false);
        RecyclerView recycler = root.findViewById(R.id.recycler_view);
        categFav = (CheckBox) root.findViewById(R.id.favorite);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false));
        int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        recycler.addItemDecoration(new GridItemDecoration(largePadding, smallPadding));
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        retrieveData();
    }
    private void createGridCategorie(CategorieResponse[] cats) {
        RecyclerView recycler = root.findViewById(R.id.recycler_view);
        CategorieAdapter adapter = new CategorieAdapter(cats,getContext());
        System.out.println("adapterCaaat:" +adapter.getItemCount());
        recycler.setAdapter(adapter);
    }

    private void retrieveData() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<CategorieResponse[]> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_CATEGORIE), headers, null, CategorieResponse[].class,
                    CategorieFragment.this::onResponse, CategorieFragment.this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }
    private void retrieveDataFavori() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<CategorieResponse[]> request = new GsonRequest<>(Request.Method.PUT, Url.path(PUT_FAVORI), headers, null, CategorieResponse[].class,
                    CategorieFragment.this::onResponseFav, CategorieFragment.this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }

    private <T> void onResponseFav(T t) {
    }


    private void onFaild(VolleyError volleyError) {
    }

    private <T> void onResponse(CategorieResponse[] categs) {
        for (int i = 0; i < categs.length; i++) {
            createGridCategorie(categs);
        }
    }
}