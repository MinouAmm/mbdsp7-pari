package mbds.tpt.pari.adapter;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import mbds.tpt.pari.R;
import mbds.tpt.pari.databinding.CardCategorieBinding;
import mbds.tpt.pari.databinding.CardDescendantBinding;
import mbds.tpt.pari.holder.CategorieHolder;
import mbds.tpt.pari.holder.DescendantHolder;
import mbds.tpt.pari.services.payload.response.CategorieResponse;
import mbds.tpt.pari.services.payload.response.DescendantsResponse;

public class DescendantAdapter extends RecyclerView.Adapter<DescendantHolder> {
    private DescendantsResponse[] cats;

    public DescendantAdapter(DescendantsResponse[] providers) {
        this.cats = providers;
    }

    @NonNull
    @Override
    public DescendantHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater       inflater     = LayoutInflater.from(parent.getContext());
        CardDescendantBinding categBinding = DataBindingUtil.inflate(inflater, R.layout.card_descendant, parent, false);
        return new DescendantHolder(categBinding, categBinding.cardDescendant);
    }

    @Override
    public void onBindViewHolder(DescendantHolder descendantHolder, int position) {
       // Categorie catg = cats.get(position);
        DescendantsResponse catg = cats[position];
        descendantHolder.getEventBinding().setUser(catg);
    }

    @Override
    public int getItemCount() {
        return cats.length;
    }
}