package mbds.tpt.pari.services.payload.response;

import java.util.ArrayList;

public class ProgrammeFicheResponse {
    private Object createdAt;
    private Object updatedAt;
    private Object updatedBy;
    private long id;
    private String dateHeurePgm;
    private String libelle;
    private ArrayList<Object> equipes;
    private Object programme;
    private Object equipe;
    private ArrayList<Object> categories;
    private Object etat;

}
