package mbds.tpt.pari.services.payload.response;

import java.util.ArrayList;

public class ProgrammeCategorieResponse {
    private Object createdAt;
    private Object createdBy;
    private long id;
    private String nomCateg;
    private ArrayList<Object> programmes;
    private Object equipe;
    private ArrayList<Object> equipes;
    private Object etat;

    public ProgrammeCategorieResponse(Object createdAt, Object createdBy, long id, String nomCateg, ArrayList<Object> programmes, Object equipe, ArrayList<Object> equipes, Object etat) {
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.id = id;
        this.nomCateg = nomCateg;
        this.programmes = programmes;
        this.equipe = equipe;
        this.equipes = equipes;
        this.etat = etat;
    }

    public ProgrammeCategorieResponse() {
    }

    public Object getEquipe() {
        return equipe;
    }

    public void setEquipe(Object equipe) {
        this.equipe = equipe;
    }

    public ArrayList<Object> getEquipes() {
        return equipes;
    }

    public void setEquipes(ArrayList<Object> equipes) {
        this.equipes = equipes;
    }

    public Object getEtat() {
        return etat;
    }

    public void setEtat(Object etat) {
        this.etat = etat;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomCateg() {
        return nomCateg;
    }

    public void setNomCateg(String nomCateg) {
        this.nomCateg = nomCateg;
    }

    public ArrayList<Object> getProgrammes() {
        return programmes;
    }

    public void setProgrammes(ArrayList<Object> programmes) {
        this.programmes = programmes;
    }


    @Override
    public String toString() {
        return "ProgrammeCategorieResponse{" +
                "createdAt=" + createdAt +
                ", createdBy=" + createdBy +
                ", id=" + id +
                ", nomCateg='" + nomCateg + '\'' +
                ", programmes=" + programmes +
                ", equipe=" + equipe +
                ", equipaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaas=" + equipes +
                ", etat=" + etat +
                '}';
    }
}

