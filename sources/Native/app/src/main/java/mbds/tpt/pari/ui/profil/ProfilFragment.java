package mbds.tpt.pari.ui.profil;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.R;
import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.models.Utilisateur;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.QrcodeResponse;

import static mbds.tpt.pari.MainActivity.bearer;
import static mbds.tpt.pari.MainActivity.userID;
import static mbds.tpt.pari.constants.PathApi.GET_DESCENDANT;
import static mbds.tpt.pari.constants.PathApi.GET_QRCODE_BY_ID;
import static mbds.tpt.pari.constants.PathApi.GET_USER_BY_ID;

public class ProfilFragment extends Fragment {

    private ProfilViewModel profilViewModel;
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
    View root;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        profilViewModel =
                ViewModelProviders.of(this).get(ProfilViewModel.class);
        root = inflater.inflate(R.layout.fragment_profil, container, false);
        retrieveDataUsers();
        retrieveData();
        return root;
    }
    private void retrieveData() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<QrcodeResponse> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_QRCODE_BY_ID+"/"+ userID +"/qrcode"), headers, null, QrcodeResponse.class,
                    ProfilFragment.this::onResponse, ProfilFragment.this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }
    public void retrieveDataUsers() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<Utilisateur> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_USER_BY_ID+"/"+ userID), headers, null, Utilisateur.class,
                    ProfilFragment.this::onResponseUsers, ProfilFragment.this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }

    private <T> void onResponseUsers(Utilisateur utilisateur) {
        TextView code= root.findViewById(R.id.codeParrain);
        code.setText(utilisateur.getCodeParrain());
    }
//    private void uploadImage(){
//        //Showing the progress dialog
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, Url.path(GET_QRCODE_BY_ID+"/"+ userID +"/qrcode"),ProfilFragment.this::onResponse, ProfilFragment.this::onFaild);
//
//        //Creating a Request Queue
//        RequestQueue requestQueue = Volley.newRequestQueue(requireContext());
//
//        //Adding request to the queue
//        requestQueue.add(stringRequest);
//        }

//    private void getImg() {
//        ImageView img= root.findViewById(R.id.qrcode);
//        String url =  Url.path(GET_QRCODE_BY_ID+"/"+ userID +"/qrcode");
//        Glide.with(context).load(url).into(img);
//    }

    private void onFaild(VolleyError volleyError) {
        System.out.println(volleyError.toString());
    }

    private <T> void onResponse(QrcodeResponse desc) {
        ImageView img= root.findViewById(R.id.qrcode);
        img.setImageBitmap(desc.getPNG());
    }
}