package mbds.tpt.pari.ui.descendant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.R;
import mbds.tpt.pari.adapter.CategorieAdapter;
import mbds.tpt.pari.adapter.DescendantAdapter;
import mbds.tpt.pari.adapter.GridItemDecoration;
import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.models.Categorie;
import mbds.tpt.pari.models.Utilisateur;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.CategorieResponse;
import mbds.tpt.pari.services.payload.response.DescendantsResponse;
import mbds.tpt.pari.ui.categorie.CategorieFragment;

import static mbds.tpt.pari.MainActivity.bearer;
import static mbds.tpt.pari.MainActivity.userID;
import static mbds.tpt.pari.constants.PathApi.GET_CATEGORIE;
import static mbds.tpt.pari.constants.PathApi.GET_DESCENDANT;
import static mbds.tpt.pari.constants.Util.GSON;

public class DescendantFragment extends Fragment {

    private DescendantViewModel descendantViewModel;
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
    View root;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        descendantViewModel =
                ViewModelProviders.of(this).get(DescendantViewModel.class);
        root = inflater.inflate(R.layout.fragment_descendant, container, false);
        RecyclerView recycler = root.findViewById(R.id.recycler_view);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false));
        int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        recycler.addItemDecoration(new GridItemDecoration(largePadding, smallPadding));
        final TextView textView = root.findViewById(R.id.title_descendant);
        descendantViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
                retrieveData();
            }
        });
        return root;
    }
    private void createGridDescendant(DescendantsResponse[] cats) {
        RecyclerView recycler = root.findViewById(R.id.recycler_view);
        DescendantAdapter adapter = new DescendantAdapter(cats);
        System.out.println("adapteeerDesc:" +adapter.getItemCount());
        recycler.setAdapter(adapter);
    }


    private void retrieveData() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<DescendantsResponse[]> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_DESCENDANT+"/"+ userID +"/descendants"), headers, null, DescendantsResponse[].class,
                    DescendantFragment.this::onResponse, DescendantFragment.this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }
    private void onFaild(VolleyError volleyError) {
    }

    private <T> void onResponse(DescendantsResponse[] desc) {
        for (int i = 0; i < desc.length; i++) {
            System.out.println(desc.length);
            createGridDescendant(desc);
        }
    }
}