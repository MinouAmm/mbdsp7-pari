package mbds.tpt.pari.ui.compte;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import mbds.tpt.pari.models.Utilisateur;
import mbds.tpt.pari.repo.UtilisateurRepo;

public class UtilisateurViewModel extends AndroidViewModel {
    private UtilisateurRepo repositories;
    private LiveData<Utilisateur> user;

    public UtilisateurViewModel(@NonNull Application application) {
        super(application);
        repositories = new UtilisateurRepo(application);
        repositories.initUtilisateurDao();
        user = repositories.getUser();
    }

    public LiveData<Utilisateur> getUtilisateur() {
        return user;
    }

    public void insert(Utilisateur user) {
        repositories.insertUtilisateur(user);
    }
}
