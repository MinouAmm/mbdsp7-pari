package mbds.tpt.pari;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.adapter.CategorieAdapter;
import mbds.tpt.pari.adapter.DescendantAdapter;
import mbds.tpt.pari.constants.Nav;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.databinding.NavHeaderMainBinding;
import mbds.tpt.pari.models.Utilisateur;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.CategorieResponse;
import mbds.tpt.pari.services.payload.response.DescendantsResponse;
import mbds.tpt.pari.ui.descendant.DescendantFragment;

import static mbds.tpt.pari.constants.PathApi.GET_CATEGORIE_BY_PARENT;
import static mbds.tpt.pari.constants.PathApi.GET_DESCENDANT;
import static mbds.tpt.pari.constants.Util.QR_CODE_EXTRA;
import static mbds.tpt.pari.constants.Util.REQUEST_QR_CODE_PARRAIN;
import static mbds.tpt.pari.constants.Util.TOKEN;
import static mbds.tpt.pari.constants.Util.USERS_ID;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    SharedPreferences sharedpreferences;
    Utilisateur users;
    public static String userID, bearer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedpreferences = getSharedPreferences(Util.SHARED_SESSION, MODE_PRIVATE);
        userID = sharedpreferences.getString(USERS_ID, null);
        bearer = sharedpreferences.getString(TOKEN, null);
        String userID = sharedpreferences.getString(USERS_ID, "");
        String email = sharedpreferences.getString("email", "");
        String username = sharedpreferences.getString("username", "");
        String role = sharedpreferences.getString("roles", "");
        try {
            setNavigation();
            users = new Utilisateur(Long.valueOf(userID),username, email, role);
//        TextView nomt = findViewById(R.id.username);
//        TextView prenomt = findViewById(R.id.email);
//        System.out.println("zzz");
//        System.out.println("*** "+users);
//        System.out.println(users.getEmail());
//        System.out.println(users.getUsername());
//        System.out.println(users.getRoles());
//        String usernom =users.getUsername();
//        String usermail =users.getEmail();
//            System.out.println("usernom " +usernom);
//            System.out.println("usermail " +usermail);
//        nomt.setText(usernom);
//        prenomt.setText(usermail);
//        System.out.println("aaa");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void progres(boolean show) {
        ProgressBar bar = findViewById(R.id.loading_spinner);
        bar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void setNavigation() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout   drawer = findViewById(R.id.drawer_layout);
        NavigationView nav    = findViewById(R.id.nav_view);

        mAppBarConfiguration = new AppBarConfiguration.Builder(Nav.NAVS_ID).setOpenableLayout(drawer).build();
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        assert navHostFragment != null;
        NavController navCo = navHostFragment.getNavController();
        NavigationUI.setupActionBarWithNavController(this, navCo, mAppBarConfiguration);
        NavigationUI.setupWithNavController(nav, navCo);
        String userID = sharedpreferences.getString(USERS_ID, "");
        String email = sharedpreferences.getString("email", "");
        String username = sharedpreferences.getString("username", "");
        String role = sharedpreferences.getString("roles", "");
        users = new Utilisateur(Long.valueOf(userID),username, email, role);
        View                 headerView = nav.getHeaderView(0);
        NavHeaderMainBinding binding    = NavHeaderMainBinding.bind(headerView);
        binding.setUser(users);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }
    public void onClickScan(View view) {
        Intent intent = new Intent(MainActivity.this, QRCodeScanner.class);
        //startActivityForResult(intent, REQUEST_QR_CODE_PARRAIN);
        startActivityForResult(intent, Integer.parseInt(userID));
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == Integer.parseInt(userID)) {
                        String           result    = data.getStringExtra(QR_CODE_EXTRA);
                        setScannStatus(users);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setScannStatus(Utilisateur users) {
        TextView view = findViewById(R.id.txtQrcodeValue);
        if (users != null ) {
            view.setText(users.getCodeParrain());
            view.setVisibility(View.VISIBLE);
            view.animate().alpha(0f).setDuration(3000).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.GONE);
                }
            });
        } else {
            assert false;
            view.setText("");
        }
    }
    /*public void onClickPlus(long idcateg) {
        final Button button = findViewById(R.id.prog);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Format the associated uri
                Uri uri = Uri.parse("idCategorie:" + findViewById(R.id.idcateg));
                // Declare the associated Intent
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);*/
//                Intent i1 = new Intent( MainActivity.this, DetailProgrammeActivity.class );
//                i1.putExtra("id",idcateg );
//                startActivityForResult(i1, 0);
//            }
//        });
//    }*/

}