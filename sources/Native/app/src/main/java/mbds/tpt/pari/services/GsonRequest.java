package mbds.tpt.pari.services;

import android.os.Build;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.services.payload.response.CategorieResponse;

import static mbds.tpt.pari.constants.Util.GSON;


public class GsonRequest<T> extends Request<T> {
    private final Map<String, String> headers;
    private final String body;
    private String contentType;
    private Class<T> clazz;
    private final Response.Listener<T> listener;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param path    URL of the request to make
     * @param headers Map of request headers
     * @param body    Map of body
     */
    public GsonRequest(int method, String path, Map<String, String> headers, String body, Class<T> clazz, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, path, errorListener);
        this.headers = headers;
        this.listener = listener;
        this.body = body;
        this.clazz = clazz;
        System.out.println(getUrl());
        /*this.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 30000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 30000;
            }

            @Override
            public void retry(VolleyError error) {

            }
        });*/
    }



    @Override
    public byte[] getBody() throws AuthFailureError {
        if (body != null && !body.equals(""))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return body.getBytes(StandardCharsets.UTF_8);
            }
        return super.getBody();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    public String getBodyContentType() {
        return contentType == null ? super.getBodyContentType() : contentType;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            System.out.println("jsooon: " + json);
            Response<T> response1= Response.success(GSON.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
            System.out.println("-----: "+response1);
            return response1;
        } catch (UnsupportedEncodingException | JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
