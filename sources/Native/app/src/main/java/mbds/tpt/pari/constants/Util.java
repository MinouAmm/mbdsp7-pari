package mbds.tpt.pari.constants;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Util {
    public static final String SHARED_SESSION         = "mbds.tpt.pari.session";
    public static final Gson GSON                   = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
    public static final String USER_CONNECTED         = "IS_USER_CONNECTE";
    public static final String ID                     = "ID_";
    public static final String USERS_ID = "id";
    public static final String TOKEN = "token";
    public static final String QR_CODE_EXTRA          = "mbds.tpt.pari.QR_CODE";
    public static final int    REQUEST_QR_CODE_PARRAIN = 1426;
    public static boolean hasValue(Object o) {
        return o != null;
    }

    public static String bearer(String token) {
        String bearer = String.format("%s %s", "Bearer", token);
        System.out.println(bearer);
        return bearer;
    }
}

