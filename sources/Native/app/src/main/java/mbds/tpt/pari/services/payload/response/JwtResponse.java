package mbds.tpt.pari.services.payload.response;

import java.util.List;

public class JwtResponse {
	private Long id;
	private String username;
	private String email;
	private List<String> roles;
	private String tokenType = "Bearer";
	private String accessToken;

	public JwtResponse() {
	}

	public JwtResponse(Long id, String username, String email, List<String> roles, String tokenType, String accessToken) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.tokenType = tokenType;
		this.accessToken = accessToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "JwtResponse{" +
				"id=" + id +
				", username='" + username + '\'' +
				", email='" + email + '\'' +
				", roles=" + roles +
				", tokenType='" + tokenType + '\'' +
				", accessToken='" + accessToken + '\'' +
				'}';
	}
}
