package mbds.tpt.pari.constants;

public class Url {
    //https://pari-rest.herokuapp.com
    //https://mbdsp7-e-fanamby.herokuapp.com
    //private static final String BASE_URL     = "http://192.168.1.100:8080";
    private static final String BASE_URL     = "https://mbdsp7-e-fanamby.herokuapp.com";
    private static final String BASE_URL_GAE = "https://pari-rest.herokuapp.com";
//    private static final String BASE_URL_GAE = "http://192.168.1.104:8080";
    public static String path(String path) {
        return BASE_URL_GAE + path;
    }
    public static String pathURL(String path) {
        return BASE_URL + path;
    }
}
