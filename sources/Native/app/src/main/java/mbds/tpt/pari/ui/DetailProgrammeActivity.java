package mbds.tpt.pari.ui;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.internal.LinkedTreeMap;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import mbds.tpt.pari.QRCodeScanner;
import mbds.tpt.pari.R;
import mbds.tpt.pari.adapter.GridItemDecoration;
import mbds.tpt.pari.adapter.ProgrammeCategorieAdapter;
import mbds.tpt.pari.constants.Nav;
import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.models.Equipe;
import mbds.tpt.pari.models.Programme;
import mbds.tpt.pari.models.Utilisateur;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.CoteByPgmResponse;
import mbds.tpt.pari.services.payload.response.ProgrammeCategorieResponse;
import mbds.tpt.pari.ui.detailpari.DetailFragment;

import static mbds.tpt.pari.constants.PathApi.GET_COTE_BY_PGM;
import static mbds.tpt.pari.constants.PathApi.GET_PROGRAMME_BY_CATEGORIE;
import static mbds.tpt.pari.constants.Util.TOKEN;
import static mbds.tpt.pari.constants.Util.USERS_ID;

public class DetailProgrammeActivity extends AppCompatActivity {
    private AppBarConfiguration mAppBarConfiguration;
    SharedPreferences sharedpreferences;
    Utilisateur users;
    public static String userID, bearer;
    final DetailFragment myFragment = new DetailFragment();
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
    long idCateg;
    long idPgm;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_programme);

        sharedpreferences = getSharedPreferences(Util.SHARED_SESSION, MODE_PRIVATE);
        userID = sharedpreferences.getString(USERS_ID, null);
        bearer = sharedpreferences.getString(TOKEN, null);
        try {
            setNavigation();
            RecyclerView recycler = findViewById(R.id.recycler_view);
            recycler.setHasFixedSize(true);
            recycler.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.VERTICAL, false));
            int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
            int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
            recycler.addItemDecoration(new GridItemDecoration(largePadding, smallPadding));
            retrieveData();
            if (savedInstanceState == null) {
                Bundle extras = getIntent().getExtras();
                if (extras == null) {
                    idCateg = 0;
                } else {
                    idCateg = (long) extras.get("id");
                    System.out.println("idCateg++++ :" + idCateg);
                    retrievedataCote();
                }
            } else {
                idCateg = (long) savedInstanceState.getSerializable("id");
                System.out.println("idCateg**** :" + idCateg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setNavigation() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView nav = findViewById(R.id.nav_view);

        mAppBarConfiguration = new AppBarConfiguration.Builder(Nav.NAVS_ID).setOpenableLayout(drawer).build();
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        assert navHostFragment != null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }

    public void onClickScan(View view) {
        Intent intent = new Intent(DetailProgrammeActivity.this, QRCodeScanner.class);
        startActivityForResult(intent, Integer.parseInt(userID));
    }

    private void createGridPgmFiche(ProgrammeCategorieResponse[] cats) {
        RecyclerView recycler = findViewById(R.id.recycler_view);
        ProgrammeCategorieAdapter adapter = new ProgrammeCategorieAdapter(cats, getApplicationContext());
        System.out.println("adapterPgm:" + adapter.getItemCount());
        recycler.setAdapter(adapter);
    }


    private void retrieveData() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<ProgrammeCategorieResponse> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_PROGRAMME_BY_CATEGORIE + "/" + idCateg + "/programmes?withEquipes=true"), headers, null, ProgrammeCategorieResponse.class,
                    DetailProgrammeActivity.this::onResponse, this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }

    private void onFaild(VolleyError volleyError) {
        System.out.println(volleyError.getMessage());
        System.out.println(volleyError);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private <T> void onResponse(ProgrammeCategorieResponse desc) {

        List<Programme> programmes = null;
        List<Equipe> lstequipes = new ArrayList<>();
        Programme programme = new Programme();
        Equipe equipe = new Equipe();
        long idcat = desc.getId();
        for (Object object : desc.getProgrammes()) {
            if (programmes == null) programmes = new ArrayList<>();
            LinkedTreeMap linkedTreeMap = (LinkedTreeMap) object;
            Date createdAt = getDateFromString((String) linkedTreeMap.get("createdAt"), "yyyy-MM-dd'T'hh:mm:ss");
            Object createdBy = linkedTreeMap.get("createdBy");
            idPgm= ((Double) linkedTreeMap.get("id")).longValue();
            String str = (String) linkedTreeMap.get("dateHeurePgm");
            Date datePg = getDateFromString(str, "yyyy-MM-dd'T'hh:mm:ss");
            String libelle = (String) linkedTreeMap.get("libelle");
            List<Equipe> equipes = (List<Equipe>) linkedTreeMap.get("equipes");
            long idequipe = 0;String nomEquipe = "",coach = "";
            for (Object eqp : equipes) {
                LinkedTreeMap resp = (LinkedTreeMap) eqp;
                LinkedTreeMap equipeeeee = (LinkedTreeMap) resp.get("equipe");
                idequipe = ((Double) equipeeeee.get("id")).longValue();
                nomEquipe = (String) equipeeeee.get("nomEquipe");
                coach = (String) equipeeeee.get("coach");
            }
            programme = new Programme(createdAt, createdBy, idPgm, datePg.toInstant().atZone(ZoneId.systemDefault())
                    .toLocalDateTime(), libelle, equipes);
            equipe = new Equipe(idequipe,nomEquipe,coach);
            programmes.add(programme);
            lstequipes.add(equipe);
        }
        setContentView(R.layout.card_programme);
        LinearLayout linearEquipe = findViewById(R.id.linearEquipe);
        LinearLayout linearCateg = findViewById(R.id.linearCateg);
        LinearLayout linearLayout = findViewById(R.id.linear);
        LinearLayout lineardate = findViewById(R.id.lineardate);
        TextView catg = findViewById(R.id.nomCateg);
        catg.setText(desc.getNomCateg());
        linearCateg.removeAllViews();
        linearCateg.addView(catg);
        TextView lstpgm = null;
        for (int i = 0; i < Objects.requireNonNull(programmes).size(); i++) {
            lstpgm = new TextView(this);
            TextView datePgm = new TextView(this);
            TextView equip = new TextView(this);
            equip.setText(lstequipes.get(i).getNomEquipe());
            linearEquipe.addView(equip);
            lstpgm.setText(programmes.get(i).getLibelle());
            datePgm.setText(String.valueOf(programmes.get(i).getDateHeurePgm()));
            linearLayout.addView(lstpgm);
            lineardate.addView(datePgm);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        assert lstpgm != null;
        lstpgm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                System.out.println("idpgmmm: "+idPgm);
                b.putLong("id", idPgm);
                myFragment.setArguments(b);
//                fragmentTransaction.show(myFragment);
                fragmentTransaction.replace(R.id.card_programme,myFragment).commit();
                //fragmentTransaction.add(R.id.card_programme, myFragment)
            }
        });
    }

    private void retrievedataCote() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<CoteByPgmResponse> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_COTE_BY_PGM + "/" + idPgm), headers, null, CoteByPgmResponse.class,
                    DetailProgrammeActivity.this::onResponseCote, this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }
    private <T> void onResponseCote(CoteByPgmResponse desc) {
        System.out.println("cotee :" +desc.toString());

    }

    private void favorit(CheckBox fav) {
        fav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                fav.setChecked(isChecked);
            }
        });
    }

    public void CategFav() {
        CheckBox fav = findViewById(R.id.favorite);

        fav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int count = 0;
                count++;
                System.out.println(count);
                favorit(fav);
                /*Appel REST API*/
                if (isChecked) {
//                    EXECUTOR_SERVICE.execute(() -> {
//                        RequestQueue queue = Volley.newRequestQueue(DetailProgrammeActivity.this);
//                        GsonRequest<JwtResponse> request = new GsonRequest<>(Request.Method.PUT, PathApi., null, GSON.toJson(inscrire), JwtResponse.class,
//                                DetailProgrammeActivity.this::onResponse, DetailProgrammeActivity.this::onFaild);
//                        request.setContentType("application/json");
//                        queue.add(request);
//                    });
                } else {
                    Toast.makeText(DetailProgrammeActivity.this, "Invalid favorie", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private Date getDateFromString(String dateS, String format) {
        Date date = null;
        try {
            date = new SimpleDateFormat(format).parse(dateS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }
}