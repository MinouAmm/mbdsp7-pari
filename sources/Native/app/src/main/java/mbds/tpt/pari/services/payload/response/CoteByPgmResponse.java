package mbds.tpt.pari.services.payload.response;

import mbds.tpt.pari.models.Parametre;
import mbds.tpt.pari.models.Type;

public class CoteByPgmResponse {
    private long id;
    private int valeur;
    private Object type;
    private long idProgramme;
    private Object parametre;
    private Object __v;

    public CoteByPgmResponse() {
    }

    public CoteByPgmResponse(long id, int valeur, Object type, long idProgramme, Object parametre, Object __v) {
        this.id = id;
        this.valeur = valeur;
        this.type = type;
        this.idProgramme = idProgramme;
        this.parametre = parametre;
        this.__v = __v;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public long getIdProgramme() {
        return idProgramme;
    }

    public void setIdProgramme(long idProgramme) {
        this.idProgramme = idProgramme;
    }

    public Object getParametre() {
        return parametre;
    }

    public void setParametre(Object parametre) {
        this.parametre = parametre;
    }

    public Object get__v() {
        return __v;
    }

    public void set__v(Object __v) {
        this.__v = __v;
    }

    @Override
    public String toString() {
        return "CoteByPgmResponse{" +
                "id=" + id +
                ", valeur=" + valeur +
                ", type=" + type +
                ", idProgramme=" + idProgramme +
                ", parametre=" + parametre +
                ", __v=" + __v +
                '}';
    }
}