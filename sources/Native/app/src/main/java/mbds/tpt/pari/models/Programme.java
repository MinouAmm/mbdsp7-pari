package mbds.tpt.pari.models;
import android.annotation.SuppressLint;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.time.LocalDateTime;
import java.util.Date;

import mbds.tpt.pari.converter.JsonConverters;

@Entity
@SuppressLint("SimpleDateFormat")
@TypeConverters(value = JsonConverters.class)
public class Programme {
    @PrimaryKey
    private Date createdAt;
    private  Object createdBy;
    private long id;
    private LocalDateTime dateHeurePgm;
    private String libelle;
    private PgmEquipe[] equipeaas;
    private Object equipes;
    private Etat etat;
    public Programme() {
    }

    public Programme(Date createdAt, Object createdBy, long id, LocalDateTime dateHeurePgm, String libelle, Object equipes) {
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.id = id;
        this.dateHeurePgm = dateHeurePgm;
        this.libelle = libelle;
        this.equipes = equipes;
    }

    public Programme(Object createdBy, long id, LocalDateTime dateHeurePgm, String libelle, PgmEquipe[] equipeaas, Etat etat) {
        this.createdBy = createdBy;
        this.id = id;
        this.dateHeurePgm = dateHeurePgm;
        this.libelle = libelle;
        this.equipeaas = equipeaas;
        this.etat = etat;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getDateHeurePgm() {
        return dateHeurePgm;
    }

    public void setDateHeurePgm(LocalDateTime dateHeurePgm) {
        this.dateHeurePgm = dateHeurePgm;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public PgmEquipe[] getEquipeaas() {
        return equipeaas;
    }

    public void setEquipeaas(PgmEquipe[] equipeaas) {
        this.equipeaas = equipeaas;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Object getEquipes() {
        return equipes;
    }

    public void setEquipes(Object equipes) {
        this.equipes = equipes;
    }
}
