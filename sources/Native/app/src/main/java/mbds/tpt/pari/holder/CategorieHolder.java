package mbds.tpt.pari.holder;

import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mbds.tpt.pari.R;
import mbds.tpt.pari.databinding.CardCategorieBinding;

public class CategorieHolder extends RecyclerView.ViewHolder {
private CardCategorieBinding categBinding;
        CheckBox chkSelected;
public CategorieHolder(CardCategorieBinding eventBinding, @NonNull View itemView) {
        super(itemView);
        this.categBinding = eventBinding;
        chkSelected = (CheckBox) itemView.findViewById(R.id.favorite);
        }

public CardCategorieBinding getEventBinding() { return categBinding; }
        public CheckBox getFav() { return chkSelected; }
}
