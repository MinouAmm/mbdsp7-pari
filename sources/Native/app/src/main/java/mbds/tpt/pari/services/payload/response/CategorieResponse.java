package mbds.tpt.pari.services.payload.response;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import mbds.tpt.pari.models.Categorie;

public class CategorieResponse {
//    private Date createdAt ;
    private Object createdBy ;
    private long id ;
    private String nomCateg ;
    private Object parent;

    public CategorieResponse() {
    }

    public CategorieResponse(Date createdAt, int[] createdBy, long id, String nomCateg, Object parent) {
//        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.id = id;
        this.nomCateg = nomCateg;
        this.parent = parent;
    }

/*    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }*/

    public Object getParent() {
        return parent;
    }

    public void setParent(Object parent) {
        this.parent = parent;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomCateg() {
        return nomCateg;
    }

    public void setNomCateg(String nomCateg) {
        this.nomCateg = nomCateg;
    }

    @Override
    public String toString() {
        return "CategorieResponse{" +
                "createdBy=" + createdBy +
                ", id=" + id +
                ", nomCateg='" + nomCateg + '\'' +
                '}';
    }
}
