package mbds.tpt.pari.repo;

import android.app.Application;

import androidx.lifecycle.LiveData;

import mbds.tpt.pari.models.Utilisateur;
import mbds.tpt.pari.repo.dao.UtilisateurDao;

public class UtilisateurRepo {
    private UtilisateurDao utilisateurDao;
    private LiveData<Utilisateur> userCompte;
    private AppDatabase _db;

    public UtilisateurRepo(Application application) {
        _db = AppDatabase.getDatabase(application);
    }

    public void initUtilisateurDao() {
        utilisateurDao = _db.utilisateurDao();
        userCompte = utilisateurDao.get();
    }

    public LiveData<Utilisateur> getUser() {
        return userCompte;
    }

    public void insertUtilisateur(final Utilisateur user) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            utilisateurDao.save(user);
        });
    }
}
