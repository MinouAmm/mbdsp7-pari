package mbds.tpt.pari.repo.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import mbds.tpt.pari.models.Categorie;

@Dao
public interface CategorieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Categorie... Categories);

    @Query("SELECT * FROM categorie")
    LiveData<List<Categorie>> getAll();

    @Query("SELECT * FROM categorie where id =:CategorieId")
    LiveData<Categorie> getById(long CategorieId);

    @Query("SELECT * FROM categorie WHERE id IN(:ids)")
    LiveData<List<Categorie>> findByIds(Long[] ids);
}
