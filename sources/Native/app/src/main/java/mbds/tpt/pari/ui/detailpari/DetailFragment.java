package mbds.tpt.pari.ui.detailpari;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mbds.tpt.pari.R;
import mbds.tpt.pari.constants.Url;
import mbds.tpt.pari.constants.Util;
import mbds.tpt.pari.models.Equipe;
import mbds.tpt.pari.services.GsonRequest;
import mbds.tpt.pari.services.payload.response.CoteByPgmResponse;
import mbds.tpt.pari.services.payload.response.ProgrammeCategorieResponse;
import mbds.tpt.pari.ui.DetailProgrammeActivity;
import mbds.tpt.pari.ui.descendant.DescendantViewModel;
import mbds.tpt.pari.ui.home.HomeFragment;

import static mbds.tpt.pari.MainActivity.bearer;
import static mbds.tpt.pari.constants.PathApi.GET_COTE_BY_PGM;
import static mbds.tpt.pari.constants.PathApi.GET_PROGRAMME_BY_CATEGORIE;
import static mbds.tpt.pari.constants.PathApi.GET_PROGRAMME_BY_ID;

public class DetailFragment extends Fragment {

    private DetailViewModel mViewModel;
    long idpgm;
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
    public static DetailFragment newInstance() {
        return new DetailFragment();
    }
    View root;
    long idequipe = 0;String nomEquipe = "",coach = "",dateHeurePgm="",libelle="";
    TextView textView,equipe,date;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        savedInstanceState = getArguments();
        if (savedInstanceState != null) {
            idpgm = savedInstanceState.getLong("id");
            System.out.println("idpgmmm :"+idpgm);
        }
        retrieveData();
        retrievedataCote();
        root = inflater.inflate(R.layout.programmefiche, container, false);
        textView = root.findViewById(R.id.programme_title);
        equipe = root.findViewById(R.id.equipe_title);
        date = root.findViewById(R.id.datePg);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        // TODO: Use the ViewModel
        savedInstanceState = getArguments();
        if (savedInstanceState != null) {
            idpgm = savedInstanceState.getLong("id");
            System.out.println("idpgmmm1 :"+idpgm);
        }
    }
    private void retrieveData() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<ProgrammeCategorieResponse> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_PROGRAMME_BY_ID + "/" + idpgm +"?all=true"), headers, null, ProgrammeCategorieResponse.class,
                    DetailFragment.this::onResponse, this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }
    private void retrievedataCote() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<CoteByPgmResponse> request = new GsonRequest<>(Request.Method.GET, Url.pathURL(GET_COTE_BY_PGM + "/" + idpgm), headers, null, CoteByPgmResponse.class,
                    DetailFragment.this::onResponseCote, Throwable::printStackTrace);
            request.setContentType("application/json");
            queue.add(request);
        });
    }
    private <T> void onResponseCote(CoteByPgmResponse desc) {
        System.out.println("cotee :" +desc);

    }

    private void onFaild(VolleyError volleyError) {
    }

    private <T> void onResponse(ProgrammeCategorieResponse pgmresp) {
        System.out.println("progggg////// : "+pgmresp);
        System.out.println("progggg////// : "+pgmresp.getEquipes().toString());
        System.out.println("prgm////// : "+pgmresp.getEquipes());
        ArrayList<Object> equipes = pgmresp.getEquipes();

        for (Object eqp : equipes) {
            LinkedTreeMap resp = (LinkedTreeMap) eqp;
            LinkedTreeMap equipeeeee = (LinkedTreeMap) resp.get("equipe");
            LinkedTreeMap programmee = (LinkedTreeMap) resp.get("programme");
            idequipe = ((Double) equipeeeee.get("id")).longValue();
            nomEquipe = (String) equipeeeee.get("nomEquipe");
            coach = (String) equipeeeee.get("coach");
            dateHeurePgm=(String) programmee.get("dateHeurePgm");
            libelle=(String) programmee.get("libelle");
            System.out.println("nomEquipe :" +nomEquipe);
            mViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
                @Override
                public void onChanged(@Nullable String s) {
                    textView.setText(libelle);
                    date.setText(dateHeurePgm);
                    equipe.setText(nomEquipe);
                }
            });
        }
        System.out.println("libelle :" +libelle);
        System.out.println("dateHeurePgm :" +dateHeurePgm);
    }
    private void parier() {
        EXECUTOR_SERVICE.execute(() -> {
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Util.bearer(bearer));
            GsonRequest<ProgrammeCategorieResponse> request = new GsonRequest<>(Request.Method.GET, Url.path(GET_PROGRAMME_BY_ID + "/" + idpgm +"?all=true"), headers, null, ProgrammeCategorieResponse.class,
                    DetailFragment.this::onResponse, this::onFaild);
            request.setContentType("application/json");
            queue.add(request);
        });
    }
}