let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let TypeCoteSchema = Schema({
    id: Number,
    titre:String,
    created_at:Date,
    modele:[{type: Schema.Types.ObjectId, ref: 'ParametreCote'}]
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('TypeCote', TypeCoteSchema);