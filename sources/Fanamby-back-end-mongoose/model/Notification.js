let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let NotificationSchema = Schema({
    id: Number,
    contenu:String,
    token:String,
    created_at:Date,
    idUtilisateur:Number
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('Notification', NotificationSchema);