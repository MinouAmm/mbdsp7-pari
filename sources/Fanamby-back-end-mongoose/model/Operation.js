let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let OperationSchema = Schema({
    id: Number,
    montant:Number,
    solde:Number,
    created_at : Date,
    utilisateur:{id:Number,username:String},
    typeOperation:{ type: Schema.Types.ObjectId, ref: 'TypeOperation'}
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('Operation', OperationSchema);