let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let PariSchema = Schema({
    id: Number,
    date:Date,
    mise:Number,
    gain:Number,
    cote:{ type: Schema.Types.ObjectId, ref: 'Cote'},
    utilisateur:{id:Number,username:String,solde:Number},
    statut:Number
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('Pari', PariSchema);