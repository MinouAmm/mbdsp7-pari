let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let AfficheCoteSchema = Schema({
    idProgramme:Number,
    typeCote:{ type: Schema.Types.ObjectId, ref: 'TypeCote'},
    cotes:[{type: Schema.Types.ObjectId, ref: 'Cote'}]

    //parametre:{ type: Schema.Types.ObjectId, ref: 'ParametreCote'}
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('AfficheCote', AfficheCoteSchema);