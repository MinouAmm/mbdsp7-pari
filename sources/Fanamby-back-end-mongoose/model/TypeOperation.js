let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let TypeOperationSchema = Schema({
    id: Number,
    libelle:String
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('TypeOperation', TypeOperationSchema);