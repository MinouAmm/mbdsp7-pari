let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ModeleCoteSchema = Schema({
    id: Number,
    typeCote:{ type: Schema.Types.ObjectId, ref: 'TypeCote'},
    parametreCote:{ type: Schema.Types.ObjectId, ref: 'ParametreCote'},
    created_at:Date
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('ModeleCote', ModeleCoteSchema);