let mongoose = require('mongoose');
//let typeCategorie = require('../model/TypeCategorie');
let Schema = mongoose.Schema;
//mongoose.model('TypeCategorie', typeCategorie);


let CategorieSchema = Schema({
    id: Number,
    nom: String,
    typeCategorie:{ type: Schema.Types.ObjectId, ref: 'TypeCategorie' }
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('Categorie', CategorieSchema);
