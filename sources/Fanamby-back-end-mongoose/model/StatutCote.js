let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let StatutCoteSchema = Schema({
    id: Number,
    libelle:String,
    created_at:Date,
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('StatutCote', StatutCoteSchema);