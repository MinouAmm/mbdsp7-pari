let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ProgrammeSchema = Schema({
    id: Number,
    libelle: String,
    date:Date,
    categorie:{ type: Schema.Types.ObjectId, ref: 'Categorie'},
    listEntite:[{ type: Schema.Types.ObjectId, ref: 'Entite'}]
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('Programme', ProgrammeSchema);
