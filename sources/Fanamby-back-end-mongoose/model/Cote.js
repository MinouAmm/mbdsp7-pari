let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let CoteSchema = Schema({
    //inCart:Boolean,
    id: Number,
    valeur:Number,
   // statut:Number,
    win:Boolean,
    created_at:Date,
    type:{ type: Schema.Types.ObjectId, ref: 'TypeCote'},
    idProgramme:Number,
    parametre:{ type: Schema.Types.ObjectId, ref: 'ParametreCote'} 
    //parametre:{ type: Schema.Types.ObjectId, ref: 'ParametreCote'}
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('Cote', CoteSchema);