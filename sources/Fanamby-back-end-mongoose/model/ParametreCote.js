let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ParametreCoteSchema = Schema({
    id: Number,
    libelle:String
});

// C'est à travers ce modèle Mongoose qu'on pourra faire le CRUD
module.exports = mongoose.model('ParametreCote', ParametreCoteSchema);