let Cote = require('../model/Cote');
const { generateId } = require('../helpers/util');

// Récupérer tous les cotes (GET)
function getCotes(req, res) {
  Cote.find().exec((err, cotes) => {
    if (err) {
      res.send(err)
    }
    console.log("cote" + cotes);
    res.send(cotes);
  });
}

function getCoteByProgramme(req, res) {
  Cote.find().where('idProgramme').equals(req.params.id).populate('type').populate('parametre').exec((err, cotes) => {
    if (err) {
      res.send(err)
    }
    console.log("cote" + cotes);
    res.send(cotes);
  });
}

//Récuperer un cote

function getCote(req,res){
  let coteId=req.params.id;
  Cote.findOne({id:coteId},(err,cote)=>{
    if(err){err.send(er)}
    res.json(cote);
  })
}


// Ajout d'un Cote (POST)
function postCote(req, res) {
  let cote = new Cote();
  cote.valeur = req.body.valeur;
  cote.win = req.body.win;
  cote.created_at = req.body.created_at;
  cote.type=req.body.type;
  cote.idProgramme=req.body.idProgramme;
  cote.parametre=req.body.parametre;

  console.log("POST cote reçu :");
  console.log(cote);

  cote.save((err) => {
    if (err) {
      res.send('cant post cote ', err);
    }
   res.json({ message: `${cote._id} saved!` })
 //res.json(cote._id)
  })
}



// Update d'un cote (PUT)
function updateCote(req, res) {
  console.log("UPDATE recu Cote : ");
  console.log(req.body);
  Cote.findByIdAndUpdate(req.body.id, req.body, {new: true}, (err, cote) => {
      if (err) {
          console.log(err);
          res.send(err)
      } else {
        res.json({message: 'updated'})
      }
  });

}

// suppression d'un Pari (DELETE)
function deleteCote(req, res) {

  Cote.findByIdAndRemove(req.params.id, (err, cote) => {
      if (err) {
          res.send(err);
      }
      res.json({message: `${cote._id} deleted`});
  })
}




module.exports = {getCotes,postCote,deleteCote,updateCote,getCote,getCoteByProgramme};
