let Operation = require('../model/Operation');
const { generateId } = require('../helpers/util');

// Récupérer tous les  Operations (GET)
function getOperations(req, res) {
  Operation.find().populate('typeOperation').populate('typeOperation').exec((err, operations) => {
    if (err) {
      res.send(err)
    }
    console.log("operation" + operations);
    res.send(operations);
  });
}



function getOperationsByJoueur(req, res) {
  Operation.find().where("utilisateur.id").equals(req.params.id).populate('typeOperation').exec((err, operations) => {
    if (err) {
      res.send(err)
    }
    console.log("operation" + operations);
    res.send(operations);
  });
}

//Récuperer une Operation

function getOperation(req,res){
  //let typeOperationId=req.params.id;
  Operation.findById(req.params.id,(err,operation)=>{
    if(err){res.send(err)}
    res.json(operation);
  })
}


// Ajout d'une Operation (POST)
function postOperation(req, res) {
  let operation = new Operation();
  operation.montant = req.body.montant;
  operation.solde = req.body.solde;
  operation.created_at = req.body.created_at;
  operation.utilisateur = req.body.utilisateur;
  operation.typeOperation = req.body.typeOperation;
  operation.motif=req.body.motif;

  console.log("POST Operation reçu :");
  console.log(operation);
  operation.save((err) => {
    if (err) {
      res.send('cant post Operation ', err);
    }
    res.json({ message: `${operation._id} ajoutée avec succès !!` })
  })
}



// Update d'une Operation (PUT)
function updateOperation(req, res) {
  console.log("UPDATE recu Operation : ");
  console.log(req.body);
  Operation.findByIdAndUpdate(req.body._id, req.body, {new: true}, (err, operation) => {
      if (err) {
          console.log(err);
          res.send(err)
      } else {
        res.json({message: 'Operation modifiée avec succès !!'})
      }
  });
}

// suppression d'une Operation (DELETE)
function deleteOperation(req, res) {
Operation.findByIdAndRemove(req.params.id, (err, operation) => {
      if (err) {
          res.send(err);
      }
      res.json({message: `${operation._id} supprimée avec succès !!`});
  })
}

module.exports = {getOperations,getOperationsByJoueur,postOperation,deleteOperation,updateOperation,getOperation};
