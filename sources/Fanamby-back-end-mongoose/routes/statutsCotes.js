let StatutCote = require('../model/StatutCote');
const { generateId } = require('../helpers/util');

// Récupérer tous les StatutCote (GET)
function getStatutCotes(req, res) {
  StatutCote.find().exec((err, statutcotes) => {
    if (err) {
      res.send(err)
    }
    console.log("Statut cote GET all" + statutcotes);
    res.send(statutcotes);
  });
}

//Récuperer un StatutCote

function getStatutCote(req,res){
  //let typecoteId=req.params.id;
  StatutCote.findById(req.params.id,(err,StatutCote)=>{
    if(err){
      res.send(err);
    }
    res.json(StatutCote);
  })
}


// Ajout d'un StatutCote (POST)
function postStatutCote(req, res) {
  let StatutCote = new StatutCote();
  StatutCote.libelle = req.body.libelle;
  StatutCote.created_at = req.body.created_at;

  console.log("POST StatutCote reçu :");
  console.log(req.body);

  StatutCote.save((err) => {
    if (err) {
      res.send('cant post typecote ', err);
    }
    res.json({ message: `${StatutCote.libelle} saved!` })
  })
}

// Update d'un StatutCote (PUT)
function updateStatutCote(req, res) {
  console.log("UPDATE recu StatutCote : ");
  console.log(req.body);
  StatutCote.findByIdAndUpdate(req.body._id, req.body, {new: true}, (err, typecote) => {
      if (err) {
          console.log(err);
          res.send(err)
      } else {
        res.json({message: 'updated'})
      }
  });

}

// suppression d'un StatutCote (DELETE)
function deleteStatutCote(req, res) {
  StatutCote.findByIdAndRemove(req.params.id, (err, StatutCote) => {
      if (err) {
          res.send(err);
      }
      res.json({message: `${StatutCote.libelle} deleted`});
  })
}


module.exports = {getStatutCotes,postStatutCote,deleteStatutCote,updateStatutCote,getStatutCote};