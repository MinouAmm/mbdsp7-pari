let ParametreCote = require('../model/ParametreCote');
const { generateId } = require('../helpers/util');

// Récupérer tous les  ParametreCotes (GET)
function getParametreCotes(req, res) {
  ParametreCote.find().exec((err, parametreCotes) => {
    if (err) {
      res.send(err)
    }
    console.log("ParametreCote" + parametreCotes);
    res.send(parametreCotes);
  });
}

//Récuperer un ParametreCote

function getParametreCote(req,res){
  //let parametreCoteId=req.params.id;
  ParametreCote.findById(req.params.id,(err,parametreCote)=>{
    if(err){res.send(err)}
    res.json(parametreCote);
  })
}


// Ajout d'un ParametreCote (POST)
function postParametreCote(req, res) {
  let parametreCote = new ParametreCote();
  parametreCote.libelle = req.body.libelle;

  console.log("POST ParametreCote reçu :");
  console.log(parametreCote);

  parametreCote.save((err) => {
    if (err) {
      res.send('cant post ParametreCote ', err);
    }
    res.json({ message: `${parametreCote.libelle} ajouté avec succès !!` })
  })
}



// Update d'un ParametreCote (PUT)
function updateParametreCote(req, res) {
  console.log("UPDATE recu ParametreCote : ");
  console.log(req.body);
  ParametreCote.findByIdAndUpdate(req.body._id, req.body, {new: true}, (err, parametreCote) => {
      if (err) {
          console.log(err);
          res.send(err)
      } else {
        res.json({message: 'Parametre cote modifié avec succès !!'})
      }
  });

}

// suppression d'un ParametreCote (DELETE)
function deleteParametreCote(req, res) {
ParametreCote.findByIdAndRemove(req.params.id, (err, parametreCote) => {
      if (err) {
          res.send(err);
      }
      res.json({message: `${parametreCote._id} supprimé avec succès !!`});
  })
}

module.exports = {getParametreCotes,postParametreCote,deleteParametreCote,updateParametreCote,getParametreCote};
