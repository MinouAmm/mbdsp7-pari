let Pari = require('../model/Pari');
const { generateId } = require('../helpers/util');
var ObjectId = require('mongodb').ObjectID;

// Récupérer tous les  Paris (GET)
function getParis(req, res) {
  Pari.find().populate('cote').exec((err, paris) => {
    if (err) {
      res.send(err)
    }
    console.log("Pari" + paris);
    res.send(paris);
  });
}


function getParisByJoueur(req, res) {
  Pari.find().where('utilisateur.id').equals(req.params.id).populate('cote').exec((err, paris) => {
    if (err) {
      res.send(err)
    }
    console.log("pari" + paris);
    res.send(paris);
  });
}

function getParisByCote(req, res) {
  Pari.find({"cote": ObjectId(req.params.id)}).exec((err, paris) => {
    if (err) {
      res.send(err)
    }
    console.log("pari" + req.params.id);
    res.send(paris);
  });
}

/*
function getParisByProgramme(req, res) {
  Pari.find({"cote.idProgramme": req.params.id}).populate('cote').exec((err, paris) => {
    if (err) {
      res.send(err)
    }
    console.log("pari" + req.params.id +" "+paris);
    res.send(paris);
  });
}*/

function getParisByProgramme(req, res) {
  Pari.find().where("cote.idProgramme").equals(req.params.id).populate('cote').exec((err, paris) => {
    if (err) {
      res.send(err)
    }
    console.log("pari" + req.params.id +" "+paris);
    res.send(paris);
  });
}



//Récuperer un pari

function getPari(req,res){
  let pariId=req.params.id;
  Pari.findOne({id:pariId},(err,pari)=>{
    if(err){res.send(err)}
    res.json(pari);
  })
}


// Ajout d'un pari (POST)
function postPari(req, res) {
  let pari = new Pari();
  pari.date = req.body.date;
  pari.mise = req.body.mise;
  pari.gain = req.body.gain;
  pari.cote=req.body.cote;
  pari.utilisateur=req.body.utilisateur;
  pari.statut=req.body.statut;
  pari.gain=req.body.gain;

  console.log("POST pari reçu :");
  console.log(pari);
  pari.save((err) => {
    if (err) {
      res.send('cant post pari ', err);
    }
    //res.json({ message: `${pari.date} saved!` })
    res.json({ message: `Pari enregistré avec succès!` })
  })
}



// Update d'un pari (PUT)
function updatePari(req, res) {
  console.log("UPDATE recu pari : ");
  console.log(req.body);
  Pari.findByIdAndUpdate(req.body._id, req.body, {new: true}, (err, pari) => {
      if (err) {
          console.log(err);
          res.send(err)
      } else {
        res.json({message: 'Pari modifié avec succès !!'})
      }
  });
}

// suppression d'un pari (DELETE)
function deletePari(req, res) {
  Pari.findByIdAndRemove(req.params.id, (err, pari) => {
      if (err) {
          res.send(err);
      }
      res.json({message: `${pari._id} supprimé avec succès !!`});
  })
}


module.exports = {getParis,postPari,deletePari,updatePari,getPari,getParisByJoueur,getParisByProgramme,getParisByCote};
