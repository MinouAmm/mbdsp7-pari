let TypeOperation = require('../model/TypeOperation');
const { generateId } = require('../helpers/util');

// Récupérer tous les  TypeOperations (GET)
function getTypeOperations(req, res) {
  TypeOperation.find().populate('cote').exec((err, typeoperations) => {
    if (err) {
      res.send(err)
    }
    console.log("typeoperation" + typeoperations);
    res.send(typeoperations);
  });
}

//Récuperer un TypeOperation

function getTypeOperation(req,res){
  //let typeOperationId=req.params.id;
  TypeOperation.findById(req.params.id,(err,typeoperation)=>{
    if(err){res.send(err)}
    res.json(typeoperation);
  })
}


// Ajout d'un TypeOperation (POST)
function postTypeOperation(req, res) {
  let typeOperation = new TypeOperation();
  typeOperation.libelle = req.body.libelle;

  console.log("POST typeOperation reçu :");
  console.log(typeOperation);

  typeOperation.save((err) => {
    if (err) {
      res.send('cant post typeOperation ', err);
    }
    res.json({ message: `${typeOperation.libelle} ajouté avec succès !!` })
  })
}



// Update d'un TypeOperation (PUT)
function updateTypeOperation(req, res) {
  console.log("UPDATE recu TypeOperation : ");
  console.log(req.body);
  TypeOperation.findByIdAndUpdate(req.body._id, req.body, {new: true}, (err, typeoperation) => {
      if (err) {
          console.log(err);
          res.send(err)
      } else {
        res.json({message: 'Type operation modifié avec succès !!'})
      }
  });

}

// suppression d'un TypeOperation (DELETE)
function deleteTypeOperation(req, res) {
TypeOperation.findByIdAndRemove(req.params.id, (err, typeoperation) => {
      if (err) {
          res.send(err);
      }
      res.json({message: `${typeoperation._id} supprimé avec succès !!`});
  })
}

module.exports = {getTypeOperations,postTypeOperation,deleteTypeOperation,updateTypeOperation,getTypeOperation};
