let TypeCote = require('../model/TypeCote');
const { generateId } = require('../helpers/util');

// Récupérer tous les typecotes (GET)
function getTypeCotes(req, res) {
  TypeCote.find().populate('modele').exec((err, typecotes) => {
    if (err) {
      res.send(err)
    }
    console.log("typecote GET all" + typecotes);
    res.send(typecotes);
  });
}

//Récuperer un typecote

function getTypeCote(req,res){
  //let typecoteId=req.params.id;
  TypeCote.findById(req.params.id,(err,typecote)=>{
    if(err){
      res.send(err);
    }
    res.json(typecote);
  })
}


// Ajout d'un typecote (POST)
function postTypeCote(req, res) {
  let typecote = new TypeCote();
  typecote.titre = req.body.titre;
  typecote.modele=req.body.modele;
  typecote.created_at = req.body.created_at;

  console.log("POST typecote reçu :");
  console.log(req.body);

  typecote.save((err) => {
    if (err) {
      res.send('cant post typecote ', err);
    }
    res.json({ message: `${typecote.titre} saved!` })
  })
}

// Update d'un typecote (PUT)
function updateTypeCote(req, res) {
  console.log("UPDATE recu typeCote : ");
  console.log(req.body);
  TypeCote.findByIdAndUpdate(req.body._id, req.body, {new: true}, (err, typecote) => {
      if (err) {
          console.log(err);
          res.send(err)
      } else {
        res.json({message: 'updated'})
      }
  });

}

// suppression d'un typecote (DELETE)
function deleteTypeCote(req, res) {
  TypeCote.findByIdAndRemove(req.params.id, (err, typecote) => {
      if (err) {
          res.send(err);
      }
      res.json({message: `${typecote._id} deleted`});
  })
}


module.exports = {getTypeCotes,postTypeCote,deleteTypeCote,updateTypeCote,getTypeCote};
