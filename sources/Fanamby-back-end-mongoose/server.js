let express = require('express');
let app = express();
let bodyParser = require('body-parser');

let pari=require('./routes/paris');
let cote=require('./routes/cotes');
let typecote=require('./routes/typesCotes');
let parametrecote=require('./routes/parametresCotes');
let operation=require('./routes/operations');
let typeoperation=require('./routes/typesOperations');
let statutcote=require('./routes/statutsCotes');




let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
//mongoose.set('debug', true);

// remplacer toute cette chaine par l'URI de connexion à votre propre base dans le cloud s
//const uri = 'mongodb+srv://mb:P7zM3VePm0caWA1L@cluster0.zqtee.mongodb.net/assignments?retryWrites=true&w=majority';
const uri = 'mongodb+srv://mahery:mahery@rakitsary.u61rm.mongodb.net/fanamby?retryWrites=true&w=majority';
//const uri = 'mongodb+srv://admin:admin1234@cluster0.ts3nl.mongodb.net/assignments?retryWrites=true&w=majority';

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
};

mongoose.connect(uri, options)
  .then(() => {
    console.log("Connecté à la base MongoDB fanamby dans le cloud !");
    console.log("at URI = " + uri);
    console.log("vérifiez with http://localhost:8010/api/paris que cela fonctionne")
  },
    err => {
      console.log('Erreur de connexion: ', err);
    });

// Pour accepter les connexions cross-domain (CORS)
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
});


// Pour les formulaires
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

let port = process.env.PORT || 8010;

// les routes
const prefix = '/api';


//------ Paris ---------///
app.route(prefix + '/paris')
  .post(pari.postPari)
  .get(pari.getParis)
  .put(pari.updatePari);

app.route(prefix + '/paris/:id')
  .get(pari.getPari)
  .delete(pari.deletePari);

app.route(prefix + '/paris/joueur/:id')
  .get(pari.getParisByJoueur);
  
app.route(prefix + '/paris/programme/:id')
  .get(pari.getParisByProgramme);

app.route(prefix + '/paris/cote/:id')
  .get(pari.getParisByCote);


//------ Cotes  ---------///
app.route(prefix + '/cotes')
  .post(cote.postCote)
  .get(cote.getCotes)
  .put(cote.updateCote);

app.route(prefix + '/cotes/:id')
  .get(cote.getCote)
  .delete(cote.deleteCote);

app.route(prefix + '/cotes/programme/:id')
  .get(cote.getCoteByProgramme)

//------ Types Cotes ---------///
app.route(prefix + '/types-cotes')
  .post(typecote.postTypeCote)
  .get(typecote.getTypeCotes)
  .put(typecote.updateTypeCote);

app.route(prefix + '/types-cotes/:id')
  .get(typecote.getTypeCote)
  .delete(typecote.deleteTypeCote);

//------ Parametres Cotes ---------///
app.route(prefix + '/parametres-cotes')
  .post(parametrecote.postParametreCote)
  .get(parametrecote.getParametreCotes)
  .put(parametrecote.updateParametreCote);

app.route(prefix + '/parametres-cotes/:id')
  .get(parametrecote.getParametreCote)
  .delete(parametrecote.deleteParametreCote);

//------ Type Operations ---------///
app.route(prefix + '/types-operations')
  .post(typeoperation.postTypeOperation)
  .get(typeoperation.getTypeOperations)
  .put(typeoperation.updateTypeOperation);

app.route(prefix + '/types-operations/:id')
  .get(typeoperation.getTypeOperation)
  .delete(typeoperation.deleteTypeOperation);

///----- Operation -------/////
app.route(prefix + '/operations')
  .post(operation.postOperation)
  .get(operation.getOperations)
  .put(operation.updateOperation);

app.route(prefix + '/operations/:id')
  .get(operation.getOperation)
  .delete(operation.deleteOperation);

app.route(prefix + '/operations/joueur/:id')
.get(operation.getOperationsByJoueur);

  ///----- Statut cote -------/////
app.route(prefix + '/statuts-cotes')
.post(statutcote.postStatutCote)
.get(statutcote.getStatutCotes)









// On démarre le serveur
app.listen(port, "0.0.0.0");
console.log('Serveur démarré sur http://localhost:' + port);

module.exports = app;


